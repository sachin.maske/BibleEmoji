ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

From Bible-eCards:
* .idea\
* .idea\compiler.xml
* .idea\copyright\
* .idea\copyright\profiles_settings.xml
* .idea\gradle.xml
* .idea\misc.xml
* .idea\modules.xml
* .idea\modules\
* .idea\modules\Bible-eCards.iml
* .idea\runConfigurations.xml
* .idea\workspace.xml
* gradle\
* gradle\wrapper\
* gradle\wrapper\gradle-wrapper.jar
* gradle\wrapper\gradle-wrapper.properties
* gradlew
* gradlew.bat
* ic_launcher-web.png
* proguard-project.txt
* settings.gradle
From facebook:
* BUCK
* build.gradle
* proguard-project.txt
From slidingmenu-library:
* LICENSE.txt
* build.gradle
* library.iml
* pom.xml
From urbanairship-lib:
* build.gradle
* proguard.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:23.4.0
gson-2.3.jar => com.google.code.gson:gson:2.3

Potentially Missing Dependency:
-------------------------------
When we replaced the following .jar files with a Gradle dependency, we
inferred the dependency version number from the filename. This
specific version may not actually be available from the repository.
If you get a build error stating that the dependency is missing, edit
the version number to for example "+" to pick up the latest version
instead. (This may require you to update your code if the library APIs
have changed.)

gson-2.3.jar => version 2.3 in com.google.code.gson:gson:2.3

Replaced Libraries with Dependencies:
-------------------------------------
The importer recognized the following library projects as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the source files in your project were of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the library replacement in the import wizard and try
again:

google-play-services_lib => [com.google.android.gms:play-services:+]

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In facebook:
* AndroidManifest.xml => facebook\src\main\AndroidManifest.xml
* assets\ => facebook\src\main\assets
* libs\bolts-android-1.1.2.jar => facebook\libs\bolts-android-1.1.2.jar
* lint.xml => facebook\lint.xml
* res\ => facebook\src\main\res\
* src\ => facebook\src\main\java\
In slidingmenu-library:
* AndroidManifest.xml => slidingmenulibrary\src\main\AndroidManifest.xml
* assets\ => slidingmenulibrary\src\main\assets
* res\ => slidingmenulibrary\src\main\res\
* src\ => slidingmenulibrary\src\main\java\
In urbanairship-lib:
* AndroidManifest.xml => urbanairshiplib\src\main\AndroidManifest.xml
* assets\ => urbanairshiplib\src\main\assets
* libs\urbanairship-lib-6.4.3.jar => urbanairshiplib\libs\urbanairship-lib-6.4.3.jar
* res\ => urbanairshiplib\src\main\res\
* src\ => urbanairshiplib\src\main\java
In Bible-eCards:
* AndroidManifest.xml => bibleeCards\src\main\AndroidManifest.xml
* assets\ => bibleeCards\src\main\assets
* libs\calldorado.jar => bibleeCards\libs\calldorado.jar
* libs\FlurryAnalytics-5.1.0.jar => bibleeCards\libs\FlurryAnalytics-5.1.0.jar
* libs\picasso-2.5.0.jar => bibleeCards\libs\picasso-2.5.0.jar
* libs\tenjin.jar => bibleeCards\libs\tenjin.jar
* libs\universal-image-loader-1.9.3.jar => bibleeCards\libs\universal-image-loader-1.9.3.jar
* lint.xml => bibleeCards\lint.xml
* res\ => bibleeCards\src\main\res\
* src\ => bibleeCards\src\main\java\
* src\com\android\vending\billing\IInAppBillingService.aidl => bibleeCards\src\main\aidl\com\android\vending\billing\IInAppBillingService.aidl

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
                                                                                             _SIGNATURE" - String containing the signature of the purchase data that
     *                                  was signed with the private key of the developer
     *                                  TODO: change this to app-specific keys.
     */
    Bundle getBuyIntent(int apiVersion, String packageName, String sku, String type,
        String developerPayload);

    /**
     * Returns the current SKUs owned by the user of the type and package name specified along with
     * purchase information and a signature of the data to be validated.
     * This will return all SKUs that have been purchased in V3 and managed items purchased using
     * V1 and V2 that have not been consumed.
     * @param apiVersion billing API version that the app is using
     * @param packageName package name of the calling app
     * @param type the type of the in-app items being requested
     *        ("inapp" for one-time purchases and "subs" for subscription).
     * @param continuationToken to be set as null for the first call, if the number of owned
     *        skus are too many, a continuationToken is returned in the response bundle.
     *        This method can be called again with the continuation token to get the next set of
     *        owned skus.
     * @return Bundle containing the following key-value pairs
     *         "RESPONSE_CODE" with int value, RESULT_OK(0) if success, other response codes on
     *              failure as listed above.
     *         "INAPP_PURCHASE_ITEM_LIST" - StringArrayList containing the list of SKUs
     *         "INAPP_PURCHASE_DATA_LIST" - StringArrayList containing the purchase information
     *         "INAPP_DATA_SIGNATURE_LIST"- StringArrayList containing the signatures
     *                                      of the purchase information
     *         "INAPP_CONTINUATION_TOKEN" - String containing a continuation token for the
     *                                      next set of in-app purchases. Only set if the
     *                                      user has more owned skus than the current list.
     */
    Bundle getPurchases(int apiVersion, String packageName, String type, String continuationToken);

    /**
     * Consume the last purchase of the given SKU. This will result in this item being removed
     * from all subsequent responses to getPurchases() and allow re-purchase of this item.
     * @param apiVersion billing API version that the app is using
     * @param packageName package name of the calling app
     * @param purchaseToken token in the purchaseINDX( 	 ���           (   �  �       C b                   ��    x b     ��    �u5��e�Ժ�E~b��u5��e��u5��e�                       c a c h e . p r o p e r t i e s       ��    � l     ��    �u5��e���٭�b��u5��e��u5��e�         