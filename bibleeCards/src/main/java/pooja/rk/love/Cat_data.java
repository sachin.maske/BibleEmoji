package pooja.rk.love;

public class Cat_data {
	private String cat_id;
	private String cat_name;
	private String cat_img;
	private String _img;
	public String get_img() {
		return _img;
	}

	public void set_img(String _img) {
		this._img = _img;
	}

	private String type;
	private String sku_id;
	private String heading_one;
	private String heading_two;
	private String heading_three;
	private String body_text;

	public String getHeading_one() {
		return heading_one;
	}

	public void setHeading_one(String heading_one) {
		this.heading_one = heading_one;
	}

	public String getHeading_two() {
		return heading_two;
	}

	public void setHeading_two(String heading_two) {
		this.heading_two = heading_two;
	}

	public String getHeading_three() {
		return heading_three;
	}

	public void setHeading_three(String heading_three) {
		this.heading_three = heading_three;
	}

	public String getBody_text() {
		return body_text;
	}

	public void setBody_text(String body_text) {
		this.body_text = body_text;
	}

	public String getSku_id() {
		return sku_id;
	}

	public void setSku_id(String sku_id) {
		this.sku_id = sku_id;
	}

	public Cat_data(String cat_id, String cat_name, String cat_img,String _img,
			String type, String sku_id, String heading_one, String heading_two,
			String heading_three, String body_text) {
		this.cat_id = cat_id;
		this.cat_name = cat_name;
		this.cat_img = cat_img;
		this._img=_img;
		this.type = type;
		this.sku_id = sku_id;
		this.heading_one=heading_one;
		this.heading_two=heading_two;
		this.heading_three=heading_three;
		this.body_text=body_text;
	}

	public String getCat_img() {
		return cat_img;
	}

	public void setCat_img(String cat_img) {
		this.cat_img = cat_img;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCat_id() {
		return cat_id;
	}

	public void setCat_id(String cat_id) {
		this.cat_id = cat_id;
	}

	public String getCat_name() {
		return cat_name;
	}

	public void setCat_name(String cat_name) {
		this.cat_name = cat_name;
	}
}
