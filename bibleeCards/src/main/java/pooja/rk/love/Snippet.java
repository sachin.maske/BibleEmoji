package pooja.rk.love;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.android.vending.billing.IInAppBillingService;

public class Snippet {
	IInAppBillingService mService;
	
	ServiceConnection mServiceConn = new ServiceConnection() {
	   @Override
	   public void onServiceDisconnected(ComponentName name) {
	       mService = null;
	   }
	
	   @Override
	   public void onServiceConnected(ComponentName name,
	      IBinder service) {
	       mService = IInAppBillingService.Stub.asInterface(service);
	   }
	};
}

