package com.bibleecard;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.Random;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;
import com.tenjin.android.TenjinSDK;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import pooja.rk.love.UserEmailFetcher;

public class SplashActivity extends Activity implements Constant_values {
	private JSONObject jsonObject;
	// REPLACE THE APP ID WITH YOUR TAPJOY APP ID.
	String tapjoyAppID = "21c12dfc-9b16-4085-ba3b-4f9821f565be";
	// REPLACE THE SECRET KEY WITH YOUR SECRET KEY.
	String tapjoySecretKey = "w0k6gCCa1JPyKCSmmTsj";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		checkCAtegory();

		String adID = getResources().getString(R.string.banner_ad_unit_id).toUpperCase(Locale.getDefault());
		System.out.println("ADID==" + adID);
		Random r = new Random();
		int randomNumber = r.nextInt(500);

		if (isOnline()) {
			new SendDBMData().execute(md5(adID), String.valueOf(randomNumber));
		} else {
			showAlert("Network Not Found", "Check Your Network And Try Again!");
		}
	}

	public String md5(String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting() && netInfo.isAvailable() && netInfo.isConnected()) {
			return true;
		}
		return false;
	}

	public void showAlert(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		builder.create();
		builder.show();
	}

	class SendDBMData extends AsyncTask<String, Void, String> {

		String responseGet = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			try {

				String getURL = "http://ad.doubleclick.net/ddm/activity/src=4745566;cat=s4hoqvtj;type=sales;dc_muid="
						+ arg0[0] + ";ord=" + arg0[1];
				System.out.println("URL===" + getURL);
				HttpClient client = new DefaultHttpClient();
				HttpGet get = new HttpGet(getURL);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				responseGet = client.execute(get, responseHandler);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (responseGet != null) {
				try {

					System.out.println("Login : " + responseGet);

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}

		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(SplashActivity.this, getResources().getString(R.string.FLURRY_KEY));
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(SplashActivity.this);
	}

	@Override
	public void onResume() {
		super.onResume();
		String apiKey = getResources().getString(R.string.TENJIN_API_KEY);
		TenjinSDK instance = TenjinSDK.getInstance(this, apiKey);
		if (instance != null) {
			instance.connect();
			// Integrate a custom event with a distinct name - ie. swiping right
			// on
			// the screen
			instance.eventWithName("Category menu button"); // Re-register
															// receivers
		} // on resume
	}

	// end
	public void checkCAtegory() {
		if (NetworkUtil.isOnline()) {
			new AsyncTask<Void, Void, Void>() {

				@Override
				protected Void doInBackground(Void... params) {
					// TODO Auto-generated method stub
					jsonObject = new HttpPost_Parser()
							.get(CHECK_SUBSCRIPTION + UserEmailFetcher.getEmail(SplashActivity.this));
					return null;
				}

				protected void onPostExecute(Void result) {
					try {
						if (jsonObject.has("success")) {
							if (jsonObject.getBoolean("success")) {
								MyApp.catSubscription = jsonObject.optString("subscription");
								Intent intent = new Intent(SplashActivity.this, ECardOfTheDayActivity.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
								startActivity(intent);
								finish();
							}
						} else if (jsonObject.has("error")) {
							Intent intent = new Intent(SplashActivity.this, ECardOfTheDayActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
							startActivity(intent);
							finish();
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				};
			}.execute();
		} else {

		}
	}
}
