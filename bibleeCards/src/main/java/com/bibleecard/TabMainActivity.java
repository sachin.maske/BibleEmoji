package com.bibleecard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;

public class TabMainActivity extends FragmentActivity {
	SlidingTabLayout mSlidingTabLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.tabmain_activity);
		ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
		// PagerTabStrip pagerTabStrip = (PagerTabStrip)
		// findViewById(R.id.pager_header);
		viewPager.setAdapter(new SampleFragmentPagerAdapter());

		mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
		mSlidingTabLayout.setDistributeEvenly(true);
		mSlidingTabLayout.setViewPager(viewPager);

	}

	public void goBack(View view) {
		finish();
	}

	public class SampleFragmentPagerAdapter extends FragmentStatePagerAdapter {
		final int PAGE_COUNT = 5;
		// Tab Titles
		private String tabtitles[] = new String[] { "Popular", "Categories", "Today�s Sponsor", "Today�s Ecard",
				"Deluxe" };

		public SampleFragmentPagerAdapter() {
			super(getSupportFragmentManager());
		}

		@Override
		public int getCount() {
			return PAGE_COUNT;
		}

		@Override
		public Fragment getItem(int position) {
			if (position == 0) {
				return PopularEmojiFragment.newInstance();
			} else if (position == 1) {
				return CategoriesFragment.newInstance();
			} else if (position == 2) {
				return TodaySponserFragment.newInstance();
			} else if (position == 3) {
				return TodayEmojiFragment.newInstance();
			} else if (position == 4) {
				return DeluxeFragment.newInstance();
			} else {
				return PopularEmojiFragment.newInstance();
			}
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return tabtitles[position];
		}
	}

}
