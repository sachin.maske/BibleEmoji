package com.bibleecard;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.tenjin.android.TenjinSDK;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import pooja.rk.love.Cat_data;
import pooja.rk.love.TransparentProgressDialog;

public class Catagories extends Activity implements Constant_values {
	private ListView listView_catagory;
	private MyCustomAdp adp;
	private SlidingMenu menu;
	private SessionManager manager;
	private AdView mAdView;
	private InterstitialAd appoftheday_interstitial, moreapp_interstitial, middle_interstitial;
	Map<String, String> menu_btn_clicked;
	ImageView imgBanner, imageDrawer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.catagory);
		load_moreApp();
		load_todayApp();
		manager = new SessionManager(Catagories.this);
		mAdView = (AdView) findViewById(R.id.adView);
		imageDrawer = (ImageView) findViewById(R.id.imageDrawer);
		AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
		mAdView.loadAd(adRequest);
		menu = new SlidingMenu(Catagories.this);
		menu.setMode(SlidingMenu.RIGHT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.side_menu);
		// moPubView.setBannerAdListener(this);
		adp = new MyCustomAdp();
		imgBanner = (ImageView) findViewById(R.id.imgBanner);

		listView_catagory = (ListView) findViewById(R.id.listView_catagory);
		if (NetworkUtil.isOnline()) {
			new GetCatagories().execute();
		} else {
			Toast.makeText(Catagories.this, "No internet connection", Toast.LENGTH_LONG).show();
		}

		Runnable mRunnable;
		Handler mHandler = new Handler();

		mRunnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				imgBanner.setVisibility(View.GONE);
			}
		};
		mHandler.postDelayed(mRunnable, 20 * 1000);
		imgBanner.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String url = "https://track.tenjin.io/v0/click/c1BicXXnqgmmo3l1tQKfvk?referrer=utm_source%3Dreferrals%26utm_campaign_id%3Dc1BicXXnqgmmo3l1tQKfvk";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
		imageDrawer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				menu.toggle();
			}
		});
	}

	public void mydailydevotion(View v) {
		String url = "https://track.tenjin.io/v0/click/esA1MVFwiKvtr3rAaVDrdD?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DesA1MVFwiKvtr3rAaVDrdD";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void biblequest(View v) {
		String url = "https://track.tenjin.io/v0/click/bSw1tFv9mDdSoH9TFERfyt?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DbSw1tFv9mDdSoH9TFERfyt";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void bvbt(View v) {
		String url = "https://track.tenjin.io/v0/click/hB8MGXL9SJzz33PE5RcgPQ?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DhB8MGXL9SJzz33PE5RcgPQ";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void bibledictionary(View v) {
		String url = "https://track.tenjin.io/v0/click/fUhcb4rPcsxR68U5QWIEw6?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DfUhcb4rPcsxR68U5QWIEw6";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void biblify(View v) {
		String url = "https://track.tenjin.io/v0/click/f8sXV6hfaLIYBg9F9DoRWo?referrer=utm_source%3Dreferrals%26utm_campaign_id%3Df8sXV6hfaLIYBg9F9DoRWo";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void bibleEmoji(View v) {
		String url = "https://track.tenjin.io/v0/click/d8uu2vyPoQwjqJmO4YcCU5?referrer=utm_source%3Dreferrals%26utm_campaign_id%3Dd8uu2vyPoQwjqJmO4YcCU5";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(Catagories.this, getResources().getString(R.string.FLURRY_KEY));
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(Catagories.this);
	}

	public void appOfTheDay(View arg1) {
		if (appoftheday_interstitial.isLoaded()) {
			appoftheday_interstitial.show();
		}
	}

	@SuppressLint("DefaultLocale")
	private class GetCatagories extends AsyncTask<Void, Void, Void> {
		JSONObject jsonobject;
		TransparentProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new TransparentProgressDialog(Catagories.this, R.drawable.progress);
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			jsonobject = new HttpPost_Parser().get(all_catagories_parser);
			return null;
		}

		@SuppressLint("DefaultLocale")
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (jsonobject.getBoolean("success")) {
					MyApp.catagory.clear();
					String one = null, two = null, three = null, body = null, img = "", cat_img = null;
					JSONArray jsonArray = jsonobject.getJSONArray("result");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						String id = object.getString("category_id");
						String name = object.getString("category_name");
						cat_img = object.getString("icon_image");
						if (object.has("purchase_image")) {
							img = object.getString("purchase_image");
						}
						System.out.println("====> " + img);
						if (cat_img.length() == 0) {
							cat_img = "http://orioninfosolutions.com/bible/admin/cat-thumb/";
						}
						if (img.length() == 0) {
							img = "http://orioninfosolutions.com/bible/admin/cat-thumb/";
						}
						String type = object.getString("type").toUpperCase();

						if (type.equals("FREE")) {
							type = "FREE";
						} else {
							if (MyApp.catSubscription.equals("active")) {
								type = "PURCHASED";
							} else if (MyApp.catSubscription.equals("inactive")) {
								type = "Deluxe";
							} else if (MyApp.catSubscription.equals("user does not exist")) {
								type = "Deluxe";
							}
						}

						String sku = object.getString("sku_id");
						if (object.has("heading1")) {
							one = object.getString("heading1");
						}
						if (object.has("heading2")) {
							two = object.getString("heading2");
						}
						if (object.has("heading3")) {
							three = object.getString("heading3");
						}
						if (object.has("body_text")) {
							body = object.getString("body_text");
						}
						MyApp.catagory.add(new Cat_data(id, name, cat_img, img, type, sku, one, two, three, body));
					}
					listView_catagory.setAdapter(adp);
				} else

				{
					// handle exception condition here
				}
			} catch (

			Exception e)

			{
				// TODO: handle exception
				e.printStackTrace();
			}

		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		try {
			String apiKey = getResources().getString(R.string.TENJIN_API_KEY);
			TenjinSDK instance = TenjinSDK.getInstance(this, apiKey);
			if (instance != null) {
				instance.connect();
				instance.eventWithName("Category click");
			}
			if (mAdView != null) {
				mAdView.resume();
			}
			super.onResume();
			if (manager.get_ctaegory_count() >= 6) {
				// show ads here
				middle_interstitial = new InterstitialAd(Catagories.this);
				middle_interstitial.setAdUnitId("ca-app-pub-1979658376370758/5817244224");
				middle_interstitial.setAdListener(new AdListener() {
					@Override
					public void onAdLoaded() {
						// TODO Auto-generated method stub
						super.onAdLoaded();
						if (middle_interstitial.isLoaded()) {
							middle_interstitial.show();
							manager.resertCatCount();
						}
					}

					@Override
					public void onAdClosed() {
						// TODO Auto-generated method stub
						super.onAdClosed();
						manager.resertCatCount();
					}

					@Override
					public void onAdFailedToLoad(int errorCode) {
						// TODO Auto-generated method stub
						super.onAdFailedToLoad(errorCode);
					}
				});
				AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

				// Begin loading your interstitial.
				middle_interstitial.loadAd(adRequestBuilder.build());
				// manager.resertCatCount();
			} else {
				manager.categoryIncrease();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void launchMarket(View v) {
		menu_btn_clicked = new HashMap<String, String>();
		menu_btn_clicked.put("clicked_menu", "Rate Us");
		FlurryAgent.logEvent("category_menu_btn", menu_btn_clicked);
		Uri uri = Uri.parse("market://details?id=" + getPackageName());
		Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			startActivity(myAppLinkToMarket);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
		}
		// menu.toggle();
	}

	public void likeUsOnFb(View view) {
		menu_btn_clicked = new HashMap<String, String>();
		menu_btn_clicked.put("clicked_menu", "Like Us On Facebook");
		FlurryAgent.logEvent("category_menu_btn", menu_btn_clicked);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("https://www.facebook.com/thebibleappproject"));
		startActivity(intent);
		// menu.toggle();
	}

	public void goBack(View view) {
		finish();
	}

	public void goVerses(View v) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(verses));
		startActivity(intent);
	}

	public void deluxe(View v) {
		Intent intent = new Intent(Catagories.this, DeluxeActivity.class);
		startActivity(intent);
	}

	public void goDevotion(View v) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(devotion));
		startActivity(intent);
	}

	public void followOnTwitter(View view) {
		menu_btn_clicked = new HashMap<String, String>();
		menu_btn_clicked.put("clicked_menu", "Follow Us On Twitter");
		FlurryAgent.logEvent("category_menu_btn", menu_btn_clicked);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("https://twitter.com/BibleAppProject"));
		startActivity(intent);
		// menu.toggle();
	}

	public void freeUpdates(View view) {
		menu_btn_clicked = new HashMap<String, String>();
		menu_btn_clicked.put("clicked_menu", "Get Free Updates");
		FlurryAgent.logEvent("category_menu_btn", menu_btn_clicked);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("http://www.thebibleappproject.org/#!news-letter/ch2n"));
		startActivity(intent);
		// menu.toggle();
	}

	private class MyCustomAdp extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return MyApp.catagory.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = LayoutInflater.from(Catagories.this).inflate(R.layout.custom_cat_view, null);
			}
			final Cat_data data = MyApp.catagory.get(position);
			TextView cat_name = (TextView) convertView.findViewById(R.id.textView_cat_name);
			TextView cat_id = (TextView) convertView.findViewById(R.id.textView_cat_id);
			ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView1);
			final Button button = (Button) convertView.findViewById(R.id.button1);
			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					String string = button.getText().toString();
					if (string.equals("FREE")) {
						Intent intent = new Intent(Catagories.this, Catagory_Detailed.class);
						intent.putExtra("cat_id", data.getCat_id());
						intent.putExtra("cat_name", data.getCat_name());
						startActivity(intent);
					} else if (string.equals("PURCHASED")) {
						Intent intent = new Intent(Catagories.this, Catagory_Detailed.class);
						intent.putExtra("cat_id", data.getCat_id());
						intent.putExtra("position", position);
						MyApp.ISFREEORPAID = "paid";
						startActivity(intent);
					} else {
						Intent intent = new Intent(Catagories.this, DeluxeActivity.class);
						startActivity(intent);
					}
				}
			});
			button.setText(data.getType());
			cat_name.setText(data.getCat_name());
			cat_id.setText(data.getCat_id());
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					menu_btn_clicked = new HashMap<String, String>();
					menu_btn_clicked.put("category_selected", data.getCat_name());
					FlurryAgent.logEvent("category_clicked", menu_btn_clicked);
					if (data.getType().equals("FREE")) {
						Intent intent = new Intent(Catagories.this, Catagory_Detailed.class);
						MyApp.ISFREEORPAID = "free";
						intent.putExtra("cat_id", data.getCat_id());
						intent.putExtra("cat_name", data.getCat_name());
						startActivity(intent);
					} else if (data.getType().equals("PURCHASED")) {
						Intent intent = new Intent(Catagories.this, Catagory_Detailed.class);
						intent.putExtra("cat_id", data.getCat_id());
						intent.putExtra("position", position);
						MyApp.ISFREEORPAID = "paid";
						startActivity(intent);
					} else {
						Intent intent = new Intent(Catagories.this, DeluxeActivity.class);
						startActivity(intent);
					}
				}
			});

			return convertView;
		}
	}

	public void load_todayApp() {
		appoftheday_interstitial = new InterstitialAd(Catagories.this);
		appoftheday_interstitial.setAdUnitId("ca-app-pub-8594467615871430/9099552107");
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		appoftheday_interstitial.loadAd(adRequestBuilder.build());
	}

	public void load_moreApp() {
		moreapp_interstitial = new InterstitialAd(Catagories.this);
		moreapp_interstitial.setAdUnitId("ca-app-pub-1979658376370758/5817244224");
		AdRequest adRequestBuilder = new AdRequest.Builder().build();
		moreapp_interstitial.loadAd(adRequestBuilder);
		moreapp_interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
				if (moreapp_interstitial.isLoaded()) {
					moreapp_interstitial.show();
				}
			}

			@Override
			public void onAdClosed() {
				// TODO Auto-generated method stub
				super.onAdClosed();
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				// TODO Auto-generated method stub
				super.onAdFailedToLoad(errorCode);

			}
		});
	}
}
