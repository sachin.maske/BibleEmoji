package com.bibleecard;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import dalvik.system.DexClassLoader;
import pooja.rk.love.Cat_data;

public class MyApp extends Application {

	public static ArrayList<Cat_data> catagory = new ArrayList<Cat_data>();
	public static ArrayList<ImgData> imgList = new ArrayList<ImgData>();
	// public static ArrayList<String> catListPurchased = new
	// ArrayList<String>();
	public static String catSubscription;
	public static String ISFREEORPAID = "free";
	public static GoogleAnalytics analytics;
	public static Tracker tracker;

	@SuppressWarnings("unused")
	@Override
	public void onCreate() {
		analytics = GoogleAnalytics.getInstance(this);
		analytics.setLocalDispatchPeriod(1800);

		tracker = analytics.newTracker("UA-62961529-30"); // Replace with actual
															// tracker/property
															// Id
		tracker.enableExceptionReporting(true);
		tracker.enableAdvertisingIdCollection(true);
		tracker.enableAutoActivityTracking(true);
		// ***********************
		FlurryAgent.setLogEnabled(false);
		FlurryAgent.init(this, getResources().getString(R.string.FLURRY_KEY));
		// TODO Auto-generated method stub
		// Parse.initialize(this, "c6Tglulzj1kXsX7VwGA9zPFDUilY8Y97IA6HCX30",
		// "agT8s3JuM1G8OCljklgHCaFwtt7zsx08qZu0K85g");
		// // Also in this method, specify a default Activity to handle push
		// notifications
		// PushService.setDefaultPushCallback(this, Home.class);
		File cacheDir = StorageUtils.getCacheDirectory(getApplicationContext());
		// Create global configuration and initialize ImageLoader with this
		// configuration
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.not_found).showImageOnFail(R.drawable.not_found).cacheInMemory(true)
				.cacheOnDisk(true).resetViewBeforeLoading(false).imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.ARGB_8888).displayer(new SimpleBitmapDisplayer()).build();
		DisplayImageOptions options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.app_icon_one) // resource
																													// or
																													// drawable
				.showImageForEmptyUri(R.drawable.not_found) // resource or
															// drawable
				.showImageOnFail(R.drawable.not_found) // resource or drawable
				.resetViewBeforeLoading(false) // default
				.delayBeforeLoading(1000).cacheInMemory(true) // default
				.cacheOnDisk(true) // default
				// .preProcessor(...)
				// .postProcessor(...)
				// .extraForDownloader(...)
				.considerExifParams(false) // default
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
				.bitmapConfig(Bitmap.Config.ARGB_8888) // default
				// .decodingOptions(...)
				.displayer(new SimpleBitmapDisplayer()) // default
				.handler(new Handler()) // default
				.build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
				.defaultDisplayImageOptions(options).memoryCache(new LruMemoryCache(2 * 1024 * 1024))
				.memoryCacheSize(2 * 1024 * 1024).memoryCacheSizePercentage(13)
				// default
				.diskCache(new UnlimitedDiscCache(cacheDir))
				// default
				.diskCacheSize(50 * 1024 * 1024).diskCacheFileCount(100).build();
		ImageLoader.getInstance().init(config);

		AirshipConfigOptions asoptions = new AirshipConfigOptions();
		asoptions.developmentAppKey = "-9v_hyP1SMytt9f_cis-mg";
		asoptions.developmentAppSecret = "Yo5_QsmETuy1v29k_J0sPQ";
		asoptions.productionAppKey = "Your Production App Key";
		asoptions.productionAppSecret = "Your Production App Secret";
		asoptions.gcmSender = "148364592656";
		asoptions.inProduction = false;

		UAirship.takeOff(this, asoptions);

		String channelId = UAirship.shared().getPushManager().getChannelId();
		Logger.info("My Application Channel ID: " + channelId);
		System.out.println("zzzzzzzzzzzzzzzzzzzzzzzzzz======" + channelId);

		UAirship.shared().getPushManager().setUserNotificationsEnabled(true);
		UAirship.shared().getPushManager().setVibrateEnabled(true);
		UAirship.shared().getPushManager().setSoundEnabled(true);

	}

}
