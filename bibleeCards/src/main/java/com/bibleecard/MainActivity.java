package com.bibleecard;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.Random;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;
import com.tenjin.android.TenjinSDK;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Window;
import pooja.rk.love.UserEmailFetcher;

public class MainActivity extends FragmentActivity implements Constant_values {
	private JSONObject jsonObject;
	// REPLACE THE APP ID WITH YOUR TAPJOY APP ID.
	String tapjoyAppID = "21c12dfc-9b16-4085-ba3b-4f9821f565be";
	// REPLACE THE SECRET KEY WITH YOUR SECRET KEY.
	String tapjoySecretKey = "w0k6gCCa1JPyKCSmmTsj";
	private static final String SEND_TAGS_STATUS_FRAGMENT_TAG = "send_tags_status_fragment_tag";
	public static final String ACTION_UPDATE_CHANNEL = "com.bibleecard.ACTION_UPDATE_CHANNEL";
	private static String tag = "OPTLY";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		// Optimizely.startOptimizelyWithAPIToken("AANKR1kBXZuR1MeVu855iXXV0RBC75rs~4303730057",
		// getApplication(),
		// mOptimizelyEventListener);

		// OPTIONAL: For custom startup flags.
		// Hashtable<String, Object> connectFlags = new Hashtable<String,
		// Object>();
		// connectFlags.put(TapjoyConnectFlag.ENABLE_LOGGING, "true");
		// TapjoyConnect.requestTapjoyConnect(this, tapjoyAppID,
		// tapjoySecretKey, connectFlags,
		// new TapjoyConnectNotifier() {
		//
		// @Override
		// public void connectSuccess() {
		// // TODO Auto-generated method stub
		// System.out.println("Tapjoy connected successfully");
		//
		// }
		//
		// @Override
		// public void connectFail() {
		// // TODO Auto-generated method stub
		// System.out.println("Tapjoy connection failed!!");
		// }
		// });

		checkCAtegory();
		/*
		 * new Handler().postDelayed(new Runnable() {
		 * 
		 * @Override public void run() { // TODO Auto-generated method stub
		 * startActivity(new Intent(MainActivity.this,Home.class)); } }, 1500);
		 */
		// try {
		// PackageInfo info =
		// getPackageManager().getPackageInfo("rkp.free.biblefy.your.screen",
		// PackageManager.GET_SIGNATURES);
		// for (Signature signature : info.signatures) {
		// MessageDigest md = MessageDigest.getInstance("SHA");
		// md.update(signature.toByteArray());
		// Log.d("KeyHash:", Base64.encodeToString(md.digest(),
		// Base64.DEFAULT));
		// System.out.println("keyhash=" + Base64.encodeToString(md.digest(),
		// Base64.DEFAULT));
		// }
		// } catch (NameNotFoundException e) {
		// e.printStackTrace();
		// } catch (NoSuchAlgorithmException e) {
		// e.printStackTrace();
		// }

		String adID = getResources().getString(R.string.banner_ad_unit_id).toUpperCase(Locale.getDefault());
		System.out.println("ADID==" + adID);
		Random r = new Random();
		int randomNumber = r.nextInt(500);

		if (isOnline()) {
			new SendDBMData().execute(md5(adID), String.valueOf(randomNumber));
		} else {
			showAlert("Network Not Found", "Check Your Network And Try Again!");
		}

	}

	// private static DefaultOptimizelyEventListener mOptimizelyEventListener =
	// new DefaultOptimizelyEventListener() {
	// public void onOptimizelyStarted() {
	// Log.i(tag, "Optimizely started.");
	// }
	//
	// public void onOptimizelyFailedToStart(String errorMessage) {
	// Log.i(tag, "Failed to start");
	// }
	//
	// public void onOptimizelyExperimentViewed(OptimizelyExperimentData
	// experimentState) {
	// Log.i(tag, "Experiment Viewed");
	// }
	//
	// public void onOptimizelyEditorEnabled() {
	// Log.i(tag, "Optimizely is ready to connect to the editor.");
	// }
	//
	// public void onOptimizelyDataFileLoaded() {
	// Log.i(tag, "Optimizely experiment data file loaded.");
	// }
	//
	// public void onGoalTriggered(String description,
	// List<OptimizelyExperimentData> affectedExperiments) {
	// Log.i(tag, "Goal Triggered");
	// }
	//
	// public void onMessage(String source, String messageType, Bundle payload)
	// {
	// Log.i(tag, "Received message");
	// }
	// };

	public String md5(String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting() && netInfo.isAvailable() && netInfo.isConnected()) {
			return true;
		}
		return false;
	}

	public void showAlert(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		builder.create();
		builder.show();
	}

	class SendDBMData extends AsyncTask<String, Void, String> {

		String responseGet = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			try {

				String getURL = "http://ad.doubleclick.net/ddm/activity/src=4745566;cat=s4hoqvtj;type=sales;dc_muid="
						+ arg0[0] + ";ord=" + arg0[1];
				System.out.println("URL===" + getURL);
				HttpClient client = new DefaultHttpClient();
				HttpGet get = new HttpGet(getURL);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				responseGet = client.execute(get, responseHandler);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (responseGet != null) {
				try {

					System.out.println("Login : " + responseGet);

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}

		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(MainActivity.this, getResources().getString(R.string.FLURRY_KEY));
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(MainActivity.this);
	}

	@Override
	public void onResume() {
		super.onResume();
		String apiKey = getResources().getString(R.string.TENJIN_API_KEY);
		TenjinSDK instance = TenjinSDK.getInstance(this, apiKey);
		if (instance != null) {
			instance.connect();
			// Integrate a custom event with a distinct name - ie. swiping right
			// on
			// the screen
			instance.eventWithName("Category menu button"); // Re-register
															// receivers
		} // on resume
	}

	// end
	public void checkCAtegory() {
		if (NetworkUtil.isOnline()) {
			new AsyncTask<Void, Void, Void>() {

				@Override
				protected Void doInBackground(Void... params) {
					// TODO Auto-generated method stub
					jsonObject = new HttpPost_Parser()
							.get(CHECK_SUBSCRIPTION + UserEmailFetcher.getEmail(MainActivity.this));
					return null;
				}

				protected void onPostExecute(Void result) {
					try {
						// MyApp.catListPurchased.clear();

						if (jsonObject.has("success")) {
							if (jsonObject.getBoolean("success")) {

								MyApp.catSubscription = jsonObject.optString("subscription");

								// JSONArray array =
								// jsonObject.getJSONArray("result");
								// for (int i = 0; i < array.length(); i++) {
								// JSONObject json = array.getJSONObject(i);
								// if
								// (!MyApp.catListPurchased.contains(json.getString("category_id")))
								// {
								// MyApp.catListPurchased.add(json.getString("category_id"));
								// }
								// }
								// System.out.println(MyApp.catListPurchased);
								if (NetworkUtil.isNotification) {
									finish();
								} else {
									startActivity(new Intent(MainActivity.this, Home.class));
								}
							}
						} else if (jsonObject.has("error")) {
							if (NetworkUtil.isNotification) {
								finish();
							} else {
								startActivity(new Intent(MainActivity.this, Home.class));
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				};
			}.execute();
		} else {

		}
	}

}
