package com.bibleecard;

import java.io.ByteArrayOutputStream;
import java.util.Map;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tenjin.android.TenjinSDK;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RK extends Activity implements Constant_values {
	private ImageView imageView;
	private Bitmap bmp;
	private Button button;
	private TextView name, link;
	private ImgData data;
	private String path;
	private ProgressBar progressBar;
	private String attribution_link = "https://creativecommons.org/licenses/by/2.0/legalcode";
	int pos;
	private SlidingMenu menu;
	private InterstitialAd appoftheday_interstitial, moreapp_interstitial;
	private Map<String, String> wall_btn_clicked;
	ImageView imageDrawer;
	private AbsoluteLayout m_alTop;
	float m_lastTouchX, m_lastTouchY, m_posX, m_posY, m_prevX, m_prevY, m_imgXB, m_imgYB, m_imgXC, m_imgYC, m_dx, m_dy;;
	private GestureDetector gestureDetector;
	TextView text;
	final static float STEP = 200;
	float mRatio = 1.0f;
	int mBaseDist;
	float mBaseRatio;
	float fontsize = 13;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.rk);
		load_moreApp();
		load_todayApp();
		imageView = (ImageView) findViewById(R.id.imageView_myimage);
		button = (Button) findViewById(R.id.button5);
		name = (TextView) findViewById(R.id.textView_name);
		link = (TextView) findViewById(R.id.textView_url);
		pos = getIntent().getExtras().getInt("pos");
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		m_prevX = 0;
		m_prevY = 0;
		m_alTop = (AbsoluteLayout) findViewById(R.id.ddalTop);
		text = (TextView) findViewById(R.id.text);
		gestureDetector = new GestureDetector(this, new SingleTapConfirm());
		if (MyApp.imgList.size() >= pos) {
			data = MyApp.imgList.get(pos);

			// menu = new SlidingMenu(RK.this);
			// menu.setMode(SlidingMenu.RIGHT);
			// menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			// menu.setShadowWidthRes(R.dimen.shadow_width);
			// menu.setShadowDrawable(R.drawable.shadow);
			// menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
			// menu.setFadeDegree(0.35f);
			// menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
			// menu.setMenu(R.layout.side_menu);
			name.setText(data.getImgName());

			link.setText(data.getLinkUrl());
			System.out.println("###" + pos);
		}
		imageDrawer = (ImageView) findViewById(R.id.imageDrawer);
		menu = new SlidingMenu(RK.this);
		menu.setMode(SlidingMenu.RIGHT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.side_menu);
		TextView textView = (TextView) findViewById(R.id.textView_attribution_link);

		textView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(attribution_link));
				startActivity(i);
			}
		});
		ImageLoader.getInstance().loadImage(data.getImgMainUrl(), new SimpleImageLoadingListener() {
			@SuppressLint("NewApi")
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				// TODO Auto-generated method stub
				imageView.setImageBitmap(loadedImage);
				bmp = loadedImage;
				// System.out.println(bmp.getByteCount());
				button.setEnabled(true);
				progressBar.setVisibility(View.GONE);
				text.setOnTouchListener(m_onTouchListener);
			}
		});
		imageDrawer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				menu.toggle();
			}
		});
	}

	private class SingleTapConfirm extends SimpleOnGestureListener {

		@Override
		public boolean onSingleTapConfirmed(MotionEvent event) {
			return true;
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			// TODO Auto-generated method stub
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(RK.this);
			// alertDialog.setTitle("PASSWORD");
			alertDialog.setMessage("Enter Text");

			final EditText input = new EditText(RK.this);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			input.setLayoutParams(lp);
			alertDialog.setView(input);

			alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					String strText = input.getText().toString();
					if (!strText.equals("")) {
						text.setText(strText);
					}
				}
			});

			alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});

			alertDialog.show();
			return true;
		}

	}

	int getDistance(MotionEvent event) {
		int dx = (int) (event.getX(0) - event.getX(1));
		int dy = (int) (event.getY(0) - event.getY(1));
		return (int) (Math.sqrt(dx * dx + dy * dy));
	}

	/**
	 * Touch listener for view
	 */
	OnTouchListener m_onTouchListener = new OnTouchListener() {

		@Override
		public boolean onTouch(View p_v, MotionEvent p_event) {
			if (p_event.getPointerCount() == 2) {
				int action = p_event.getAction();
				int pureaction = action & MotionEvent.ACTION_MASK;
				if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
					mBaseDist = getDistance(p_event);
					mBaseRatio = mRatio;
				} else {
					float delta = (getDistance(p_event) - mBaseDist) / STEP;
					float multi = (float) Math.pow(2, delta);
					mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
					text.setTextSize(mRatio + 13);
				}
			} else {
				gestureDetector.onTouchEvent(p_event);
				drag(p_event, text);
				// switch (p_event.getAction()) {
				// case MotionEvent.ACTION_DOWN:
				// m_lastTouchX = p_event.getX();
				// m_lastTouchY = p_event.getY();
				// break;
				// case MotionEvent.ACTION_UP:
				// break;
				//
				// case MotionEvent.ACTION_MOVE:
				// m_dx = p_event.getX() - m_lastTouchX;
				// m_dy = p_event.getY() - m_lastTouchY;
				//
				// m_posX = m_prevX + m_dx;
				// m_posY = m_prevY + m_dy;
				//
				// if (m_posX > 0 && m_posY > 0 && (m_posX + p_v.getWidth()) <
				// m_alTop.getWidth()
				// && (m_posY + p_v.getHeight()) < m_alTop.getHeight()) {
				// p_v.setLayoutParams(new
				// AbsoluteLayout.LayoutParams(p_v.getMeasuredWidth(),
				// p_v.getMeasuredHeight(), (int) m_posX, (int) m_posY));
				//
				// m_prevX = m_posX;
				// m_prevY = m_posY;
				//
				// break;
				// }
				// }
			}
			return true;
		}
	};

	public void drag(MotionEvent event, View v) {
		AbsoluteLayout.LayoutParams params = (android.widget.AbsoluteLayout.LayoutParams) v.getLayoutParams();

		switch (event.getAction()) {
		case MotionEvent.ACTION_MOVE: {
			m_dx = event.getX() - m_lastTouchX;
			m_dy = event.getY() - m_lastTouchY;

			m_posX = m_prevX + m_dx;
			m_posY = m_prevY + m_dy;

			if (m_posX > 0 && m_posY > 0 && (m_posX + v.getWidth()) < m_alTop.getWidth()
					&& (m_posY + v.getHeight()) < m_alTop.getHeight()) {
				v.setLayoutParams(new AbsoluteLayout.LayoutParams(v.getMeasuredWidth(), v.getMeasuredHeight(),
						(int) m_posX, (int) m_posY));

				m_prevX = m_posX;
				m_prevY = m_posY;
				// params.topMargin = (int) event.getY() - (v.getHeight());
				// params.leftMargin = (int) event.getX() - (v.getWidth() / 2);
				// v.setLayoutParams(params);
				break;
			}
		}
		case MotionEvent.ACTION_UP: {
//			params.topMargin = (int) event.getY() - (v.getHeight());
//			params.leftMargin = (int) event.getX() - (v.getWidth() / 2);
//			v.setLayoutParams(params);
			break;
		}
		case MotionEvent.ACTION_DOWN: {
			m_lastTouchX = event.getX();
			m_lastTouchY = event.getY();
			// v.setLayoutParams(params);
			break;
		}

		}
	}

	public void mydailydevotion(View v) {
		String url = "https://track.tenjin.io/v0/click/esA1MVFwiKvtr3rAaVDrdD?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DesA1MVFwiKvtr3rAaVDrdD";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void biblequest(View v) {
		String url = "https://track.tenjin.io/v0/click/bSw1tFv9mDdSoH9TFERfyt?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DbSw1tFv9mDdSoH9TFERfyt";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void bvbt(View v) {
		String url = "https://track.tenjin.io/v0/click/hB8MGXL9SJzz33PE5RcgPQ?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DhB8MGXL9SJzz33PE5RcgPQ";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void bibledictionary(View v) {
		String url = "https://track.tenjin.io/v0/click/fUhcb4rPcsxR68U5QWIEw6?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DfUhcb4rPcsxR68U5QWIEw6";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void biblify(View v) {
		String url = "https://track.tenjin.io/v0/click/f8sXV6hfaLIYBg9F9DoRWo?referrer=utm_source%3Dreferrals%26utm_campaign_id%3Df8sXV6hfaLIYBg9F9DoRWo";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void bibleEmoji(View v) {
		String url = "https://track.tenjin.io/v0/click/d8uu2vyPoQwjqJmO4YcCU5?referrer=utm_source%3Dreferrals%26utm_campaign_id%3Dd8uu2vyPoQwjqJmO4YcCU5";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(RK.this, getResources().getString(R.string.FLURRY_KEY));
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(RK.this);
	}

	public void showFullPageAd(View view) {
		if (moreapp_interstitial.isLoaded()) {
			moreapp_interstitial.show();
		}
	}

	public void appOfTheDay(View arg1) {
		if (appoftheday_interstitial.isLoaded()) {
			appoftheday_interstitial.show();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		String apiKey = getResources().getString(R.string.TENJIN_API_KEY);
		TenjinSDK instance = TenjinSDK.getInstance(this, apiKey);
		if (instance != null) {
			instance.connect();
			// Integrate a custom event with a distinct name - ie. swiping right
			// on
			// the screen
			instance.eventWithName("wallpaper change");
			// com.facebook.AppEventsLogger.activateApp(getApplicationContext(),
			// "813006208712433");
		}

	}

	public void load_todayApp() {
		appoftheday_interstitial = new InterstitialAd(RK.this);
		appoftheday_interstitial.setAdUnitId("ca-app-pub-8594467615871430/9099552107");
		appoftheday_interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();

			}

			@Override
			public void onAdClosed() {
				// TODO Auto-generated method stub
				super.onAdClosed();
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				// TODO Auto-generated method stub
				super.onAdFailedToLoad(errorCode);
				Toast.makeText(getBaseContext(), "check internet connection", Toast.LENGTH_SHORT).show();
			}
		});
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

		// Optionally populate the ad request builder.
		// put here your device id
		adRequestBuilder.addTestDevice("4A1FDD7238AC4953C53783E9819BA7EB");

		// Begin loading your interstitial.
		appoftheday_interstitial.loadAd(adRequestBuilder.build());
	}

	public void load_moreApp() {
		moreapp_interstitial = new InterstitialAd(RK.this);
		moreapp_interstitial.setAdUnitId("ca-app-pub-8594467615871430/3053018508");
		moreapp_interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();

			}

			@Override
			public void onAdClosed() {
				// TODO Auto-generated method stub
				super.onAdClosed();
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				// TODO Auto-generated method stub
				super.onAdFailedToLoad(errorCode);
				Toast.makeText(getBaseContext(), "check internet connection", Toast.LENGTH_SHORT).show();
			}
		});
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

		// Optionally populate the ad request builder.
		// put here your device id
		adRequestBuilder.addTestDevice("4A1FDD7238AC4953C53783E9819BA7EB");

		// Begin loading your interstitial.
		moreapp_interstitial.loadAd(adRequestBuilder.build());
	}

	public void launchMarket(View v) {
		Uri uri = Uri.parse("market://details?id=" + getPackageName());
		Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			startActivity(myAppLinkToMarket);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
		}
		// menu.toggle();
	}

	public void likeUsOnFb(View view) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("https://www.facebook.com/thebibleappproject"));
		startActivity(intent);
		// menu.toggle();
	}

	public void followOnTwitter(View view) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("https://twitter.com/BibleAppProject"));
		startActivity(intent);
		// menu.toggle();
	}

	public void freeUpdates(View view) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("http://www.thebibleappproject.org/#!news-letter/ch2n"));
		startActivity(intent);
		// menu.toggle();
	}

	// public void showMenu(View view) {
	// menu.toggle();
	// }

	public void goBack(View view) {
		onBackPressed();
	}

	@SuppressLint("NewApi")
	public void setWallpaper(View view) {
		try {
			m_alTop.buildDrawingCache();
			Bitmap bmap = m_alTop.getDrawingCache();
			// sharing eomoji
			if (bmap != null) {
				String pathofBmp = Images.Media.insertImage(getContentResolver(), bmap, data.getImgName(), null);
				// saveImage(bmp);
				Uri bmpUri = Uri.parse(pathofBmp);
				if (bmpUri != null) {
					final Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
					emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					emailIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);
					emailIntent1.putExtra(Intent.EXTRA_TEXT, "http://www.FunBibleEcards.com/");
					emailIntent1.setType("image/png");
					startActivity(Intent.createChooser(emailIntent1, "Share via ..."));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// void saveImage(Bitmap bitmap) {
	//
	// String root = Environment.getExternalStorageDirectory().toString();
	// File myDir = new File(root + "/saved_images");
	//
	// myDir.mkdir();
	//
	// String fname = "Image.png";
	// File file = new File (myDir, fname);
	// if (file.exists ())
	// file.delete ();
	// try {
	// FileOutputStream out = new FileOutputStream(file);
	// bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
	// out.flush();
	// out.close();
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// @SuppressLint("NewApi")
	// public static Bitmap eraseColor(Bitmap src, int color) {
	// int width = src.getWidth();
	// int height = src.getHeight();
	// Bitmap b = src.copy(Config.ARGB_8888, true);
	// b.setHasAlpha(true);
	//
	// int[] pixels = new int[width * height];
	// src.getPixels(pixels, 0, width, 0, 0, width, height);
	//
	// for (int i = 0; i < width * height; i++) {
	// if (pixels[i] == color) {
	// pixels[i] = 0;
	// }
	// }
	//
	// b.setPixels(pixels, 0, width, 0, 0, width, height);
	//
	// return b;
	// }

	public Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		path = Images.Media.insertImage(inContext.getContentResolver(), inImage, data.getImgName(), null);
		return Uri.parse(path);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (bmp != null) {
			bmp.recycle();
		}

		RK.this.finish();
	}

	public void goVerses(View v) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(verses));
		startActivity(intent);
	}

	public void deluxe(View v) {
		Intent intent = new Intent(RK.this, DeluxeActivity.class);
		startActivity(intent);
	}

	public void goDevotion(View v) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(devotion));
		startActivity(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RESULT_OK && requestCode == 100) {
			Toast.makeText(RK.this, "Wallpaper changed", Toast.LENGTH_SHORT).show();
		}
	}
}
