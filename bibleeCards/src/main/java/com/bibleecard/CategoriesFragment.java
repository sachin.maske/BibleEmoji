package com.bibleecard;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import pooja.rk.love.Cat_data;
import pooja.rk.love.TransparentProgressDialog;

public class CategoriesFragment extends Fragment {
	private ListView listView_catagory;
	private MyCustomAdp adp;
	Map<String, String> menu_btn_clicked;
	NativeExpressAdView mNativeExpressAdView;

	public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container,
			android.os.Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_catagories, container, false);

		LinearLayout layout = (LinearLayout) view.findViewById(R.id.linearAds);

		// Create a native express ad. The ad size and ad unit ID must be set
		// before calling
		// loadAd.
		mNativeExpressAdView = new NativeExpressAdView(getActivity());
		mNativeExpressAdView.setAdSize(new AdSize(AdSize.FULL_WIDTH, 100));
		mNativeExpressAdView.setAdUnitId("ca-app-pub-1979658376370758/9524104227");
		// Create an ad request.
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		// adRequestBuilder.addTestDevice("2FB882443B07AD2E8BFE4CBCECE28556");

		// Add the NativeExpressAdView to the view hierarchy.
		layout.addView(mNativeExpressAdView);

		// Start loading the ad.
		mNativeExpressAdView.loadAd(adRequestBuilder.build());

		adp = new MyCustomAdp();

		listView_catagory = (ListView) view.findViewById(R.id.listView_catagory);
		if (NetworkUtil.isOnline()) {
			new GetCatagories().execute();
		} else {
			Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
		}

		return view;
	}

	public static Fragment newInstance() {
		// TODO Auto-generated method stub
		CategoriesFragment fragmentFirst = new CategoriesFragment();

		return fragmentFirst;
	};

	@SuppressLint("DefaultLocale")
	private class GetCatagories extends AsyncTask<Void, Void, Void> {
		JSONObject jsonobject;
		TransparentProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new TransparentProgressDialog(getActivity(), R.drawable.progress);
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			jsonobject = new HttpPost_Parser().get(Constant_values.all_catagories_parser);
			return null;
		}

		@SuppressLint("DefaultLocale")
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (jsonobject.getBoolean("success")) {
					MyApp.catagory.clear();
					String one = null, two = null, three = null, body = null, img = "", cat_img = null;
					JSONArray jsonArray = jsonobject.getJSONArray("result");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						String id = object.getString("category_id");
						String name = object.getString("category_name");
						cat_img = object.getString("icon_image");
						if (object.has("purchase_image")) {
							img = object.getString("purchase_image");
						}
						System.out.println("====> " + img);
						if (cat_img.length() == 0) {
							cat_img = "http://orioninfosolutions.com/bible/admin/cat-thumb/";
						}
						if (img.length() == 0) {
							img = "http://orioninfosolutions.com/bible/admin/cat-thumb/";
						}
						String type = object.getString("type").toUpperCase();
						if (type.equals("FREE")) {
							type = "FREE";
						} else {
							if (MyApp.catSubscription.equals("active")) {
								type = "PURCHASED";
							} else if (MyApp.catSubscription.equals("inactive")) {
								type = "Deluxe";
							} else if (MyApp.catSubscription.equals("user does not exist")) {
								type = "Deluxe";
							}
						}
						String sku = object.getString("sku_id");
						if (object.has("heading1")) {
							one = object.getString("heading1");
						}
						if (object.has("heading2")) {
							two = object.getString("heading2");
						}
						if (object.has("heading3")) {
							three = object.getString("heading3");
						}
						if (object.has("body_text")) {
							body = object.getString("body_text");
						}
						MyApp.catagory.add(new Cat_data(id, name, cat_img, img, type, sku, one, two, three, body));
					}
					listView_catagory.setAdapter(adp);
				} else {
					// handle exception condition here
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private class MyCustomAdp extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return MyApp.catagory.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = LayoutInflater.from(getActivity()).inflate(R.layout.custom_cat_view, null);
			}
			final Cat_data data = MyApp.catagory.get(position);
			TextView cat_name = (TextView) convertView.findViewById(R.id.textView_cat_name);
			TextView cat_id = (TextView) convertView.findViewById(R.id.textView_cat_id);
			ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView1);
			final Button button = (Button) convertView.findViewById(R.id.button1);
			button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					String string = button.getText().toString();
					if (string.equals("FREE")) {
						Intent intent = new Intent(getActivity(), Catagory_Detailed.class);
						intent.putExtra("cat_id", data.getCat_id());
						intent.putExtra("cat_name", data.getCat_name());
						startActivity(intent);
					} else if (string.equals("PURCHASED")) {
						Intent intent = new Intent(getActivity(), Catagory_Detailed.class);
						intent.putExtra("cat_id", data.getCat_id());
						intent.putExtra("position", position);
						MyApp.ISFREEORPAID = "paid";
						startActivity(intent);
					} else {
						Intent intent = new Intent(getActivity(), DeluxeActivity.class);
						startActivity(intent);
					}
				}
			});
			button.setText(data.getType());
			cat_name.setText(data.getCat_name());
			cat_id.setText(data.getCat_id());
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					menu_btn_clicked = new HashMap<String, String>();
					menu_btn_clicked.put("category_selected", data.getCat_name());
					FlurryAgent.logEvent("category_clicked", menu_btn_clicked);
					if (data.getType().equals("FREE")) {
						Intent intent = new Intent(getActivity(), Catagory_Detailed.class);
						MyApp.ISFREEORPAID = "free";
						intent.putExtra("cat_id", data.getCat_id());
						intent.putExtra("cat_name", data.getCat_name());
						startActivity(intent);
					} else if (data.getType().equals("PURCHASED")) {
						Intent intent = new Intent(getActivity(), Catagory_Detailed.class);
						intent.putExtra("cat_id", data.getCat_id());
						intent.putExtra("position", position);
						MyApp.ISFREEORPAID = "paid";
						startActivity(intent);
					} else {
						Intent intent = new Intent(getActivity(), DeluxeActivity.class);
						startActivity(intent);
					}
				}
			});
			return convertView;
		}

	}

}
