package com.bibleecard;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class SessionManager{
	private Context context;
	private SharedPreferences preferences;
	private Editor editor;
	public SessionManager(Context ctx)
	{
		context=ctx;
		preferences=context.getSharedPreferences("rk", 0);
		editor=preferences.edit();
	}
//now methods
	public void homeIncrease()
	{
		int c=preferences.getInt("home_count", 0);
		editor.putInt("home_count", c+1);
		editor.commit();
	}
	//now methods
		public void categoryIncrease()
		{
			int c=preferences.getInt("cat_count", 0);
			editor.putInt("cat_count", c+1);
			editor.commit();
		}
//	get counts
		public int get_home_count()
		{
			return preferences.getInt("home_count", 0);
		}
		
		public int get_ctaegory_count()
		{
			return preferences.getInt("cat_count", 0);
		}
		
//		reset cunters
		public void resetHomeCount()
		{
			editor.putInt("home_count",0);
			editor.commit();
		}
		public void resertCatCount()
		{
			editor.putInt("cat_count",0);
			editor.commit();
		}
}
