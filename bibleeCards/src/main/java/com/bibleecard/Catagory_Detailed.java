package com.bibleecard;

import org.json.JSONArray;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.squareup.picasso.Picasso;
import com.tenjin.android.TenjinSDK;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import pooja.rk.love.TransparentProgressDialog;

public class Catagory_Detailed extends Activity implements Constant_values {
	private String cat_id;
	private GridView gridView;
	// private SlidingMenu menu;
	ImageView imageDrawer;
	private SlidingMenu menu;
	NativeExpressAdView mNativeExpressAdView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.catagory_detailed);
		LinearLayout layout = (LinearLayout) findViewById(R.id.linearAds);

		// Create a native express ad. The ad size and ad unit ID must be set
		// before calling
		// loadAd.
		mNativeExpressAdView = new NativeExpressAdView(Catagory_Detailed.this);
		mNativeExpressAdView.setAdSize(new AdSize(AdSize.FULL_WIDTH, 100));
		mNativeExpressAdView.setAdUnitId("ca-app-pub-1979658376370758/9524104227");
		// Create an ad request.
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		adRequestBuilder.addTestDevice("2FB882443B07AD2E8BFE4CBCECE28556");

		// Add the NativeExpressAdView to the view hierarchy.
		layout.addView(mNativeExpressAdView);

		// Start loading the ad.
		mNativeExpressAdView.loadAd(adRequestBuilder.build());

		cat_id = getIntent().getExtras().getString("cat_id");
		gridView = (GridView) findViewById(R.id.gridView1);
		imageDrawer = (ImageView) findViewById(R.id.imageDrawer);

		menu = new SlidingMenu(Catagory_Detailed.this);
		menu.setMode(SlidingMenu.RIGHT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.side_menu);

		imageDrawer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				menu.toggle();
			}
		});

		if (NetworkUtil.isOnline()) {
			new Get_Cat_Images().execute();
		} else {
			Toast.makeText(Catagory_Detailed.this, "No internet connection", Toast.LENGTH_LONG).show();
		}

	}

	public void mydailydevotion(View v) {
		String url = "https://track.tenjin.io/v0/click/esA1MVFwiKvtr3rAaVDrdD?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DesA1MVFwiKvtr3rAaVDrdD";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void biblequest(View v) {
		String url = "https://track.tenjin.io/v0/click/bSw1tFv9mDdSoH9TFERfyt?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DbSw1tFv9mDdSoH9TFERfyt";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void bvbt(View v) {
		String url = "https://track.tenjin.io/v0/click/hB8MGXL9SJzz33PE5RcgPQ?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DhB8MGXL9SJzz33PE5RcgPQ";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void bibledictionary(View v) {
		String url = "https://track.tenjin.io/v0/click/fUhcb4rPcsxR68U5QWIEw6?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DfUhcb4rPcsxR68U5QWIEw6";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void biblify(View v) {
		String url = "https://track.tenjin.io/v0/click/f8sXV6hfaLIYBg9F9DoRWo?referrer=utm_source%3Dreferrals%26utm_campaign_id%3Df8sXV6hfaLIYBg9F9DoRWo";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public void bibleEmoji(View v) {
		String url = "https://track.tenjin.io/v0/click/d8uu2vyPoQwjqJmO4YcCU5?referrer=utm_source%3Dreferrals%26utm_campaign_id%3Dd8uu2vyPoQwjqJmO4YcCU5";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		String apiKey = getResources().getString(R.string.TENJIN_API_KEY);
		TenjinSDK instance = TenjinSDK.getInstance(this, apiKey);
		if (instance != null) {
			instance.connect();
			// Integrate a custom event with a distinct name - ie. swiping right
			// on
			// the screen
			instance.eventWithName("categories-details");
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(Catagory_Detailed.this, getResources().getString(R.string.FLURRY_KEY));
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(Catagory_Detailed.this);
	}

	public void goBack(View view) {
		finish();
	}

	private class Get_Cat_Images extends AsyncTask<Void, Void, Void> {
		JSONObject json;
		TransparentProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new TransparentProgressDialog(Catagory_Detailed.this, R.drawable.progress);
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			json = new HttpPost_Parser().get(selected_cat_parser + cat_id + part + MyApp.ISFREEORPAID);
			System.out.println(selected_cat_parser + cat_id + part + MyApp.ISFREEORPAID);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				if (json.getBoolean("success")) {
					MyApp.imgList.clear();
					JSONArray array = json.getJSONArray("result");
					for (int i = 0; i < array.length(); i++) {
						JSONObject object = array.getJSONObject(i);
						MyApp.imgList.add(new ImgData(object.getString("img_title"), object.getString("thumb_url"),
								object.getString("main_url"), object.getString("link_url")));
					}
					gridView.setAdapter(new MyAdapter());
				} else {
					// handle false condition
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();

			}
		}

	}

	private class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return MyApp.imgList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(final int position, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = LayoutInflater.from(Catagory_Detailed.this).inflate(R.layout.imag, null);
			}
			ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView1);
			ImgData imgData = MyApp.imgList.get(position);
			System.out.println("Image URL===" + imgData.getImgThumbUrl());
			Picasso.with(Catagory_Detailed.this).load(imgData.getImgThumbUrl()).placeholder(R.drawable.app_icon_one)
					.error(R.drawable.not_found).into(imageView);

			imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(Catagory_Detailed.this, RK.class);
					i.putExtra("pos", position);
					startActivity(i);
				}
			});
			return convertView;
		}
	}

	public void goHome(View v) {
		onBackPressed();
	}

}
