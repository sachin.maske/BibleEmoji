package com.bibleecard;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AppoftheDayActivity extends Activity {

	ArrayList<AppListItem> arrayAppList;
	SharedPreferences pref;
	Editor editor;

	TextView textAppName, textDescription, textDate, textGetItFree;
	ImageView imageApp;
	String url;
	NativeExpressAdView mNativeExpressAdView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.appoftheday_activity);

		LinearLayout layout = (LinearLayout) findViewById(R.id.linearAds);

		// Create a native express ad. The ad size and ad unit ID must be set
		// before calling
		// loadAd.
		mNativeExpressAdView = new NativeExpressAdView(AppoftheDayActivity.this);
		mNativeExpressAdView.setAdSize(new AdSize(AdSize.FULL_WIDTH, 100));
		mNativeExpressAdView.setAdUnitId("ca-app-pub-1979658376370758/9524104227");
		// Create an ad request.
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		// adRequestBuilder.addTestDevice("2FB882443B07AD2E8BFE4CBCECE28556");

		// Add the NativeExpressAdView to the view hierarchy.
		layout.addView(mNativeExpressAdView);

		// Start loading the ad.
		mNativeExpressAdView.loadAd(adRequestBuilder.build());

		textAppName = (TextView) findViewById(R.id.textAppName);
		textDescription = (TextView) findViewById(R.id.textDescription);
		textDate = (TextView) findViewById(R.id.textDate);
		textGetItFree = (TextView) findViewById(R.id.textGetItFree);
		imageApp = (ImageView) findViewById(R.id.imageApp);

		arrayAppList = new ArrayList<AppListItem>();
		arrayAppList.add(new AppListItem(0, "My Daily Devotion", R.drawable.devotion_app_icon,
				"Be inspired with your Free Daily Devotion! Each day you will receive a Morning Prayer and a Morning Bible Verse with our #1 App.  You will also get access to deluxe prayer guides and a deluxe prayer each day! Start your day with God.",
				"https://track.tenjin.io/v0/click/cJK9wXZoattLMgRD9r7PFA?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DcJK9wXZoattLMgRD9r7PFA"));
		arrayAppList.add(new AppListItem(1, "Bible Verses By Topic", R.drawable.topic,
				"Bible Verses by Topic is another one of our free apps! It provides hundreds of Bible verses separated into 30+ different categories.  You can quickly and easily reference this app to review popular verses broken out by      Freedom, Anxiety, Love and More!",
				"https://track.tenjin.io/v0/click/dTBtCVJUpNtiPiYvn7xeZU?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DdTBtCVJUpNtiPiYvn7xeZU"));
		arrayAppList.add(new AppListItem(2, "Bible Dictionary", R.drawable.dictionary,
				"The Bible Dictionary features thousands of definitions about some of the most religious words featured in the Bible.  The definitions also provide the verses in which they�re referenced.  Get more educated with this easy to use app!",
				"https://track.tenjin.io/v0/click/bwBRhj5Vsj6aB6FDreZinD?referrer=utm_source%3Dreferrals%26utm_campaign_id%3DbwBRhj5Vsj6aB6FDreZinD"));
		arrayAppList.add(new AppListItem(3, "Weather Nun", R.drawable.nun,
				"Stay informed and Inspired with the Weather Nun! She provides current local weather AND gives you a new prayer or verse every day! This fun religious weather app gives you current conditions, 5 day forecast and hourly weather.",
				"https://track.tenjin.io/v0/click/b48Q4mNeb70RRwNiHlDQDE?referrer=utm_source%3Dreferrals%26utm_campaign_id%3Db48Q4mNeb70RRwNiHlDQDE"));
		try {
			pref = getSharedPreferences("emojiads", MODE_PRIVATE);
			editor = pref.edit();
			String strDate = pref.getString("date", "");
			int id = pref.getInt("adsid", 0);
			Calendar c = Calendar.getInstance();
			System.out.println("Current time => " + c.getTime());

			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String currentDate = df.format(c.getTime());

			if (strDate.equals("")) {
				editor.putString("date", currentDate);
				editor.putInt("adsid", arrayAppList.get(0).getId());
				textAppName.setText(arrayAppList.get(0).getName());
				Date newDate = df.parse(currentDate);
				SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
				textDate.setText(format.format(newDate));
				textDescription.setText(arrayAppList.get(0).getDescription());
				imageApp.setBackgroundResource(arrayAppList.get(0).getIcon());
				url = arrayAppList.get(0).getLink();

			} else {
				Date lastDate = null;
				lastDate = df.parse(strDate);

				if (lastDate.equals(df.parse(currentDate))) {
					textAppName.setText(arrayAppList.get(id).getName());
					Date newDate = df.parse(currentDate);
					SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
					textDate.setText(format.format(newDate));
					textDescription.setText(arrayAppList.get(id).getDescription());
					imageApp.setBackgroundResource(arrayAppList.get(id).getIcon());
					url = arrayAppList.get(id).getLink();
				} else {
					editor.putString("date", currentDate);

					if (id < arrayAppList.size() - 1) {
						id++;
					} else {
						id = 0;
					}
					editor.putInt("adsid", id);
					textAppName.setText(arrayAppList.get(id).getName());
					Date newDate = df.parse(currentDate);
					SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
					textDate.setText(format.format(newDate));
					textDescription.setText(arrayAppList.get(id).getDescription());
					imageApp.setBackgroundResource(arrayAppList.get(id).getIcon());
					url = arrayAppList.get(id).getLink();
				}
			}
			editor.commit();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		textGetItFree.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
	}

	public void goBack(View view) {
		finish();
	}
}
