package com.bibleecard;

public class AppListItem {

	int id;
	String name;
	int icon;
	String description;
	String link;

	public AppListItem(int id, String name, int icon, String description, String link) {
		super();
		this.id = id;
		this.name = name;
		this.icon = icon;
		this.description = description;
		this.link = link;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
