package com.bibleecard;

import java.util.HashMap;
import java.util.Map;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.tenjin.android.TenjinSDK;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

public class Home extends Activity {
	private SessionManager manager;
	Map<String, String> home_btn_clicked;
	AppEventsLogger logger;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.home);

		manager = new SessionManager(Home.this);
		home_btn_clicked = new HashMap<String, String>();

		Intent intent = new Intent();
		intent.setAction("com.calldorado.android.intent.INITSDK");
		intent.setPackage(this.getApplicationContext().getPackageName());
		sendBroadcast(intent);
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(Home.this, getResources().getString(R.string.FLURRY_KEY));
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(Home.this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AppEventsLogger.deactivateApp(getApplicationContext(), getResources().getString(R.string.FB_APP_ID));
	}

	public void goCategories(View view) {
		// home_btn_clicked.put("clicked_btn", "Backgrounds");
		// FlurryAgent.logEvent("home_button_events", home_btn_clicked);
		// startActivity(new Intent(Home.this, Catagories.class));
		startActivity(new Intent(Home.this, TabMainActivity.class));
	}

	public void goPrayer(View arg1) {
		String url = "https://play.google.com/store/apps/details?id=com.dailydevotionapp&hl=en";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		String apiKey = getResources().getString(R.string.TENJIN_API_KEY);
		TenjinSDK instance = TenjinSDK.getInstance(this, apiKey);
		if (instance != null) {
			instance.connect();
			// Integrate a custom event with a distinct name - ie. swiping right
			// on
			// the screen
			instance.eventWithName("Home button");
		}
		FacebookSdk.sdkInitialize(getApplicationContext());
		logger = AppEventsLogger.newLogger(getApplicationContext(), getResources().getString(R.string.FB_APP_ID));
		AppEventsLogger.activateApp(getApplicationContext(), getResources().getString(R.string.FB_APP_ID));
		System.out.println("======" + manager.get_home_count());
		if (manager.get_home_count() >= 6) {
			final InterstitialAd middle_interstitial = new InterstitialAd(Home.this);
			middle_interstitial.setAdUnitId("ca-app-pub-1979658376370758/5817244224");
			middle_interstitial.setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					// TODO Auto-generated method stub
					super.onAdLoaded();
					if (middle_interstitial.isLoaded()) {
						middle_interstitial.show();
						manager.resetHomeCount();
					}
				}

				@Override
				public void onAdClosed() {
					// TODO Auto-generated method stub
					super.onAdClosed();
					manager.resetHomeCount();
				}

				@Override
				public void onAdFailedToLoad(int errorCode) {
					// TODO Auto-generated method stub
					super.onAdFailedToLoad(errorCode);

				}
			});
			AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

			// Optionally populate the ad request builder.
			// put here your device id
			adRequestBuilder.addTestDevice("4A1FDD7238AC4953C53783E9819BA7EB");

			// Begin loading your interstitial.
			middle_interstitial.loadAd(adRequestBuilder.build());
		} else {
			manager.homeIncrease();
		}
	}

	public void goAppOfTheDay(View view) {
		startActivity(new Intent(Home.this, AppoftheDayActivity.class));
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finish();
	}
}
