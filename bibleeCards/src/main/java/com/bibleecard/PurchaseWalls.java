package com.bibleecard;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.facebook.appevents.AppEventsLogger;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.rkp.billing.IabHelper;
import com.rkp.billing.IabResult;
import com.rkp.billing.Purchase;
import com.squareup.picasso.Picasso;
import com.tenjin.android.TenjinSDK;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import pooja.rk.love.Cat_data;
import pooja.rk.love.UserEmailFetcher;

public class PurchaseWalls extends Activity implements Constant_values {
	private InterstitialAd appoftheday_interstitial, moreapp_interstitial, middle_interstitial;
	// private SlidingMenu menu;
	private SessionManager manager;
	Map<String, String> menu_btn_clicked;
	private TextView heading1, heading2, heading3, bodyText;
	private Button buyItem;
	private ImageView image;
	JSONObject jsonObject;
	// PRODUCT & SUBSCRIPTION IDS
	private String cat_id, sku_id = "";
	// private static final String PRODUCT_ID = "biblyfy.your.screen.paid.one";
	private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAirdnkErMKAs/hz/zhw/696qsO8KmautpjoN6zGmNecqWSRkbJAXqFE8rXuVCyCxw94/vgQdkiZ8zTpFvRBds+zjTN9WrTgMYd8BX/PupA3LkVY3JPVbUPE+RhLCbVhf7c5dx9lg/wFQ+I0K5E8YasWsMpqbCpknyVH/HjymEgndOygddApPUpGA9/ngNEkM2bh+aEwsEBXtT/KyuPrR1dvIcr+PRzsAVTIGQw/0A3qSX5w+T/peVo8uoRrVvew5h+RASY0SnDSDio6Hch00Lt9eftI9XlGl/a6IXhzPapNYkJSxsuzOztJAVRARe+dt5XFQ7ya5DKH/Ak4lTb8YVbwIDAQAB"; // PUT
	// The helper object
	IabHelper mHelper;
	// Debug tag, for logging
	static final String TAG = "BIBLEECARDAPP";
	// check ready or not
	private boolean is_ready = false;
	// (arbitrary) request code for the purchase flow
	static final int RC_REQUEST = 1505;
	private Cat_data cat_data;
	// ImageView imgBanner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.purchase_walls);
		load_moreApp();
		load_todayApp();
		cat_id = getIntent().getExtras().getString("cat_id");
		cat_data = MyApp.catagory.get(getIntent().getExtras().getInt("position"));
		sku_id = cat_data.getSku_id();
		System.out.println("SKU ID ===" + sku_id);
		manager = new SessionManager(PurchaseWalls.this);
		// mAdView = (AdView) findViewById(R.id.adView);
		// AdRequest adRequest = new
		// AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
		// mAdView.loadAd(adRequest);
		// menu = new SlidingMenu(PurchaseWalls.this);
		// menu.setMode(SlidingMenu.RIGHT);
		// menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		// menu.setShadowWidthRes(R.dimen.shadow_width);
		// menu.setShadowDrawable(R.drawable.shadow);
		// menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		// menu.setFadeDegree(0.35f);
		// menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		// menu.setMenu(R.layout.side_menu);
		heading1 = (TextView) findViewById(R.id.textView2);
		heading2 = (TextView) findViewById(R.id.textView3);
		heading3 = (TextView) findViewById(R.id.textView4);
		bodyText = (TextView) findViewById(R.id.textView5);
		heading1.setText(cat_data.getCat_name());
		heading2.setText(cat_data.getHeading_two());
		heading3.setText(cat_data.getHeading_three());
		bodyText.setText(cat_data.getBody_text());
		buyItem = (Button) findViewById(R.id.button1);
		image = (ImageView) findViewById(R.id.imageView11);
		System.out.println("Image===" + cat_data.get_img());
		Picasso.with(PurchaseWalls.this).load(cat_data.get_img()).error(R.drawable.app_icon_one).into(image);
		System.out.println(cat_data.getCat_img());
		buyItem.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				System.out.println("clicked outside");
				if (is_ready) {
					System.out.println("clicked");
					mHelper.launchPurchaseFlow(PurchaseWalls.this, sku_id, RC_REQUEST, mPurchaseFinishedListener);
				}
			}
		});
		mHelper = new IabHelper(this, LICENSE_KEY);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				Log.d(TAG, "Setup finished.");
				System.out.println("Setup finished.");
				if (!result.isSuccess()) {
					// Oh noes, there was a problem.
					Toast.makeText(PurchaseWalls.this, "Problem setting up in-app billing: " + result,
							Toast.LENGTH_LONG).show();
					is_ready = false;
					return;
				}

				// Have we been disposed of in the meantime? If so, quit.
				if (mHelper == null) {
					return;
				}

				// IAP is fully set up. Now, let's get an inventory of stuff we
				// own.
				Log.d(TAG, "Setup successful. Querying inventory.");
				is_ready = true;
				// mHelper.queryInventoryAsync(mGotInventoryListener);
			}
		});
		// imgBanner = (ImageView) findViewById(R.id.imgBanner);
		// Runnable mRunnable;
		// Handler mHandler = new Handler();
		//
		// mRunnable = new Runnable() {
		//
		// @Override
		// public void run() {
		// // TODO Auto-generated method stub
		// imgBanner.setVisibility(View.GONE);
		// }
		// };
		// mHandler.postDelayed(mRunnable, 20 * 1000);
		// imgBanner.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// String url =
		// "https://track.tenjin.io/v0/click/c1BicXXnqgmmo3l1tQKfvk?referrer=utm_source%3Dreferrals%26utm_campaign_id%3Dc1BicXXnqgmmo3l1tQKfvk";
		// Intent i = new Intent(Intent.ACTION_VIEW);
		// i.setData(Uri.parse(url));
		// startActivity(i);
		// }
		// });
	}

	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
			// if we were disposed of in the meantime, quit.
			if (mHelper == null)
				return;

			if (result.isFailure()) {
				return;
			}
			Log.d(TAG, "Purchase successful.");
			AppEventsLogger appEventsLogger = AppEventsLogger.newLogger(getApplicationContext(),
					getResources().getString(R.string.FB_APP_ID));
			appEventsLogger.logEvent("category_purchased");
			upgradeItem();
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(PurchaseWalls.this, getResources().getString(R.string.FLURRY_KEY));
	}

	public void upgradeItem() {
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				String email = UserEmailFetcher.getEmail(PurchaseWalls.this);
				jsonObject = new HttpPost_Parser().get(add_to_database + cat_id + last_part + email);
				return null;
			}

			protected void onPostExecute(Void result) {
				try {
					if (jsonObject.getBoolean("success")) {
						Toast.makeText(PurchaseWalls.this, "Successfully purchased", Toast.LENGTH_SHORT).show();
						Intent intent = new Intent(PurchaseWalls.this, Catagory_Detailed.class);
						intent.putExtra("cat_id", cat_data.getCat_id());
						intent.putExtra("cat_name", cat_data.getCat_name());
						startActivity(intent);
						finish();
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			};
		}.execute();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (mHelper != null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
			// not handled, so handle it ourselves (here's where you'd
			// perform any handling of activity results not related to in-app
			// billing...
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(PurchaseWalls.this);
	}

	public void showFullPageAd(View view) {
		if (moreapp_interstitial.isLoaded()) {
			moreapp_interstitial.show();
		}
	}

	public void appOfTheDay(View arg1) {
		if (appoftheday_interstitial.isLoaded()) {
			appoftheday_interstitial.show();
		}
	}

	public void goVerses(View v) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(verses));
		startActivity(intent);
	}

	public void deluxe(View v) {
		Intent intent = new Intent(PurchaseWalls.this, DeluxeActivity.class);
		startActivity(intent);
	}

	public void goDevotion(View v) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(devotion));
		startActivity(intent);
	}

	// @Override
	// protected void onPause() {
	// // TODO Auto-generated method stub
	// if (mAdView != null) {
	// mAdView.pause();
	// }
	// super.onPause();
	// }

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finish();
	}

	public void goBack(View view) {
		onBackPressed();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		// if (mAdView != null) {
		// mAdView.destroy();
		// }
		super.onDestroy();
		// very important:
		Log.d(TAG, "Destroying helper.");
		if (mHelper != null) {
			mHelper.dispose();
			mHelper = null;
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		String apiKey = getResources().getString(R.string.TENJIN_API_KEY);
		TenjinSDK instance = TenjinSDK.getInstance(this, apiKey);
		if (instance != null) {
			instance.connect();
			// Integrate a custom event with a distinct name - ie. swiping right
			// on
			// the screen
			instance.eventWithName("purchage-page");
		}
		// if (mAdView != null) {
		// mAdView.resume();
		// }
		super.onResume();
		if (manager.get_ctaegory_count() >= 8) {
			// show ads here
			middle_interstitial = new InterstitialAd(PurchaseWalls.this);
			middle_interstitial.setAdUnitId("ca-app-pub-8594467615871430/4529751707");
			middle_interstitial.setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					// TODO Auto-generated method stub
					super.onAdLoaded();
					if (middle_interstitial.isLoaded()) {
						middle_interstitial.show();
						manager.resertCatCount();
					}
				}

				@Override
				public void onAdClosed() {
					// TODO Auto-generated method stub
					super.onAdClosed();
					manager.resertCatCount();
				}

				@Override
				public void onAdFailedToLoad(int errorCode) {
					// TODO Auto-generated method stub
					super.onAdFailedToLoad(errorCode);
				}
			});
			AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

			// Optionally populate the ad request builder.
			// put here your device id
			adRequestBuilder.addTestDevice("4A1FDD7238AC4953C53783E9819BA7EB");

			// Begin loading your interstitial.
			middle_interstitial.loadAd(adRequestBuilder.build());
			// manager.resertCatCount();
		} else {
			manager.categoryIncrease();
		}
	}

	public void launchMarket(View v) {
		menu_btn_clicked = new HashMap<String, String>();
		menu_btn_clicked.put("clicked_menu", "Rate Us");
		FlurryAgent.logEvent("category_menu_btn", menu_btn_clicked);
		Uri uri = Uri.parse("market://details?id=" + getPackageName());
		Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			startActivity(myAppLinkToMarket);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
		}
		// menu.toggle();
	}

	// public void goPremium(View view) {
	// Uri uri = Uri.parse("market://details?id=" + "rk.biblefy.your.screen");
	// Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
	// try {
	// startActivity(myAppLinkToMarket);
	// } catch (ActivityNotFoundException e) {
	// Toast.makeText(this, " unable to find market app",
	// Toast.LENGTH_LONG).show();
	// }
	// menu.toggle();
	// }

	public void likeUsOnFb(View view) {
		menu_btn_clicked = new HashMap<String, String>();
		menu_btn_clicked.put("clicked_menu", "Like Us On Facebook");
		FlurryAgent.logEvent("category_menu_btn", menu_btn_clicked);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("https://www.facebook.com/thebibleappproject"));
		startActivity(intent);
		// menu.toggle();
	}

	public void followOnTwitter(View view) {
		menu_btn_clicked = new HashMap<String, String>();
		menu_btn_clicked.put("clicked_menu", "Follow Us On Twitter");
		FlurryAgent.logEvent("category_menu_btn", menu_btn_clicked);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("https://twitter.com/BibleAppProject"));
		startActivity(intent);
		// menu.toggle();
	}

	public void freeUpdates(View view) {
		menu_btn_clicked = new HashMap<String, String>();
		menu_btn_clicked.put("clicked_menu", "Get Free Updates");
		FlurryAgent.logEvent("category_menu_btn", menu_btn_clicked);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("http://www.thebibleappproject.org/#!news-letter/ch2n"));
		startActivity(intent);
		// menu.toggle();
	}

	// public void showMenu(View view) {
	// menu.toggle();
	// }

	public void load_todayApp() {
		appoftheday_interstitial = new InterstitialAd(PurchaseWalls.this);
		appoftheday_interstitial.setAdUnitId("ca-app-pub-8594467615871430/9099552107");
		appoftheday_interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();

			}

			@Override
			public void onAdClosed() {
				// TODO Auto-generated method stub
				super.onAdClosed();
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				// TODO Auto-generated method stub
				super.onAdFailedToLoad(errorCode);
				Toast.makeText(getBaseContext(), "check internet connection", Toast.LENGTH_SHORT).show();
			}
		});
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

		// Optionally populate the ad request builder.
		// put here your device id
		adRequestBuilder.addTestDevice("4A1FDD7238AC4953C53783E9819BA7EB");

		// Begin loading your interstitial.
		appoftheday_interstitial.loadAd(adRequestBuilder.build());
	}

	public void load_moreApp() {
		moreapp_interstitial = new InterstitialAd(PurchaseWalls.this);
		moreapp_interstitial.setAdUnitId("ca-app-pub-8594467615871430/3053018508");
		moreapp_interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();

			}

			@Override
			public void onAdClosed() {
				// TODO Auto-generated method stub
				super.onAdClosed();
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				// TODO Auto-generated method stub
				super.onAdFailedToLoad(errorCode);
				Toast.makeText(getBaseContext(), "check internet connection", Toast.LENGTH_SHORT).show();
			}
		});
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

		// Optionally populate the ad request builder.
		// put here your device id
		adRequestBuilder.addTestDevice("4A1FDD7238AC4953C53783E9819BA7EB");

		// Begin loading your interstitial.
		moreapp_interstitial.loadAd(adRequestBuilder.build());
	}
}
