package com.bibleecard;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore.Images;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import pooja.rk.love.TransparentProgressDialog;

public class TodayEmojiFragment extends Fragment {
	Button buttonShare;
	ImageView imageECard;
	ArrayList<String> arrayCategoryId;
	ArrayList<String> arrayECard;
	NativeExpressAdView mNativeExpressAdView;
	TransparentProgressDialog dialog;

	public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container,
			android.os.Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_todayecard, container, false);

		LinearLayout layout = (LinearLayout) view.findViewById(R.id.linearAds);

		// Create a native express ad. The ad size and ad unit ID must be set
		// before calling
		// loadAd.
		mNativeExpressAdView = new NativeExpressAdView(getActivity());
		mNativeExpressAdView.setAdSize(new AdSize(AdSize.FULL_WIDTH, 100));
		mNativeExpressAdView.setAdUnitId("ca-app-pub-1979658376370758/9524104227");
		// Create an ad request.
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		adRequestBuilder.addTestDevice("2FB882443B07AD2E8BFE4CBCECE28556");

		// Add the NativeExpressAdView to the view hierarchy.
		layout.addView(mNativeExpressAdView);

		// Start loading the ad.
		mNativeExpressAdView.loadAd(adRequestBuilder.build());

		buttonShare = (Button) view.findViewById(R.id.buttonShare);
		imageECard = (ImageView) view.findViewById(R.id.imageECard);

		if (NetworkUtil.isOnline()) {
			new GetEmojiOfTheDay().execute();
		} else {
			Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
		}
		buttonShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					imageECard.buildDrawingCache();
					Bitmap bmap = imageECard.getDrawingCache();
					// sharing eomoji
					if (bmap != null) {
						String pathofBmp = Images.Media.insertImage(getActivity().getContentResolver(), bmap, "", null);
						// saveImage(bmp);
						Uri bmpUri = Uri.parse(pathofBmp);
						if (bmpUri != null) {
							final Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
							emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							emailIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);
							emailIntent1.putExtra(Intent.EXTRA_TEXT, "http://www.FunBibleEcards.com/");
							emailIntent1.setType("image/png");
							startActivity(Intent.createChooser(emailIntent1, "Share via..."));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		return view;
	}

	public static Fragment newInstance() {
		// TODO Auto-generated method stub
		TodayEmojiFragment fragmentFirst = new TodayEmojiFragment();

		return fragmentFirst;
	};

	private class GetEmojiOfTheDay extends AsyncTask<Void, Void, Void> {
		JSONObject jsonobject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new TransparentProgressDialog(getActivity(), R.drawable.progress);
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			jsonobject = new HttpPost_Parser().get(Constant_values.ecard_of_the_day);
			return null;
		}

		@SuppressLint("DefaultLocale")
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				// 11/26/2015
				if (jsonobject.getBoolean("success")) {
					JSONArray jsonArray = jsonobject.getJSONArray("result");
					JSONObject object = jsonArray.getJSONObject(0);
					String img_id = object.getString("img_id");
					String emoji_of_day_date = object.getString("ecard_of_day_date");
					String thumb_url = object.getString("thumb_url");
					String main_url = object.getString("main_url");

					System.out.println("Date===" + emoji_of_day_date);

					try {

						Calendar c = Calendar.getInstance();

						SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

						Date date1 = formatter.parse(emoji_of_day_date);

						String currentDate = formatter.format(c.getTime());

						Date date2 = formatter.parse(currentDate);

						System.out.println("date1" + date1);
						System.out.println("date2" + date2);

						if (date1.equals(date2)) {
							ImageLoader.getInstance().loadImage(main_url, new SimpleImageLoadingListener() {
								@SuppressLint("NewApi")
								@Override
								public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
									// TODO Auto-generated method stub
									imageECard.setImageBitmap(loadedImage);
								}
							});
						} else {
							if (NetworkUtil.isOnline()) {
								new GetCatagories().execute();
							} else {
								Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
							}
						}

					} catch (ParseException e1) {
						e1.printStackTrace();
					}

				} else {
					dialog.dismiss();
					// handle exception condition here
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	private class GetCatagories extends AsyncTask<Void, Void, Void> {
		JSONObject jsonobject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			jsonobject = new HttpPost_Parser().get(Constant_values.all_catagories_parser);
			return null;
		}

		@SuppressLint("DefaultLocale")
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				if (jsonobject.getBoolean("success")) {
					arrayCategoryId = new ArrayList<String>();
					JSONArray jsonArray = jsonobject.getJSONArray("result");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						String id = object.getString("category_id");

						String type = object.getString("type").toUpperCase();
						if (type.equals("FREE")) {
							arrayCategoryId.add(id);
						}
					}

					Random r = new Random();

					System.out.println("CategorySize===" + arrayCategoryId.size());
					System.out.println("RandomCategoryID===" + arrayCategoryId.get(r.nextInt(arrayCategoryId.size())));

					if (NetworkUtil.isOnline()) {
						new Get_Cat_Images(arrayCategoryId.get(r.nextInt(arrayCategoryId.size()))).execute();
					} else {
						Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
					}
				} else {
					dialog.dismiss();
					// handle exception condition here
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private class Get_Cat_Images extends AsyncTask<Void, Void, Void> {
		JSONObject json;
		String categoryId;

		public Get_Cat_Images(String categoryId) {
			// TODO Auto-generated constructor stub
			this.categoryId = categoryId;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			json = new HttpPost_Parser()
					.get(Constant_values.selected_cat_parser + categoryId + Constant_values.part + "free");
			System.out.println(Constant_values.selected_cat_parser + categoryId + Constant_values.part + "free");
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (json.getBoolean("success")) {
					arrayECard = new ArrayList<String>();
					JSONArray array = json.getJSONArray("result");
					for (int i = 0; i < array.length(); i++) {
						JSONObject object = array.getJSONObject(i);
						arrayECard.add(object.getString("main_url"));
					}

					if (arrayECard.size() > 0) {
						Random r = new Random();

						System.out.println("RandomCategoryID===" + arrayECard.get(r.nextInt(arrayECard.size())));
						ImageLoader.getInstance().loadImage(arrayECard.get(r.nextInt(arrayECard.size())),
								new SimpleImageLoadingListener() {
									@SuppressLint("NewApi")
									@Override
									public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
										// TODO Auto-generated method stub
										imageECard.setImageBitmap(loadedImage);
									}
								});
					}
				} else {
					dialog.dismiss();
					// handle false condition
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();

			}
		}

	}

}
