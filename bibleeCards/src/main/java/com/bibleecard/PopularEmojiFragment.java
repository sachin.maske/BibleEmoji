package com.bibleecard;

import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;
import com.squareup.picasso.Picasso;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import pooja.rk.love.TransparentProgressDialog;

public class PopularEmojiFragment extends Fragment {
	private NativeExpressAdView mNativeExpressAdView;
	ArrayList<String> arrayCategoryId;
	ArrayList<String> arrayECard;
	ArrayList<String> arrayPopularECard;
	GridView gridPopularECard;
	TransparentProgressDialog dialog;

	public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container,
			android.os.Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_popular, container, false);
		LinearLayout layout = (LinearLayout) view.findViewById(R.id.linearAds);

		// Create a native express ad. The ad size and ad unit ID must be set
		// before calling
		// loadAd.
		mNativeExpressAdView = new NativeExpressAdView(getActivity());
		mNativeExpressAdView.setAdSize(new AdSize(AdSize.FULL_WIDTH, 100));
		mNativeExpressAdView.setAdUnitId("ca-app-pub-1979658376370758/9524104227");
		// Create an ad request.
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		// adRequestBuilder.addTestDevice("2FB882443B07AD2E8BFE4CBCECE28556");

		// Add the NativeExpressAdView to the view hierarchy.
		layout.addView(mNativeExpressAdView);

		// Start loading the ad.
		mNativeExpressAdView.loadAd(adRequestBuilder.build());

		gridPopularECard = (GridView) view.findViewById(R.id.gridPopularECard);

		if (NetworkUtil.isOnline()) {
			new GetCatagories().execute();
		} else {
			Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
		}

		return view;
	}

	private class GetCatagories extends AsyncTask<Void, Void, Void> {
		JSONObject jsonobject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new TransparentProgressDialog(getActivity(), R.drawable.progress);
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			jsonobject = new HttpPost_Parser().get(Constant_values.all_catagories_parser);
			return null;
		}

		@SuppressLint("DefaultLocale")
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				if (jsonobject.getBoolean("success")) {
					arrayCategoryId = new ArrayList<String>();
					JSONArray jsonArray = jsonobject.getJSONArray("result");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						String id = object.getString("category_id");

						String type = object.getString("type").toUpperCase();
						if (type.equals("FREE")) {
							arrayCategoryId.add(id);
						}
					}
					arrayECard = new ArrayList<String>();
					for (int i = 0; i < arrayCategoryId.size(); i++) {
						boolean isLast = false;
						if (i == arrayCategoryId.size() - 1) {
							isLast = true;
						}
						new Get_Cat_Images(arrayCategoryId.get(i), isLast).execute();
					}

				} else {
					dialog.dismiss();
					// handle exception condition here
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private class Get_Cat_Images extends AsyncTask<Void, Void, Void> {
		JSONObject json;
		boolean isLast;
		String categoryId;

		public Get_Cat_Images(String categoryId, boolean isLast) {
			// TODO Auto-generated constructor stub
			this.categoryId = categoryId;
			this.isLast = isLast;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			json = new HttpPost_Parser()
					.get(Constant_values.selected_cat_parser + categoryId + Constant_values.part + "free");
			System.out.println(Constant_values.selected_cat_parser + categoryId + Constant_values.part + "free");
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				if (json.getBoolean("success")) {

					JSONArray array = json.getJSONArray("result");
					for (int i = 0; i < array.length(); i++) {
						JSONObject object = array.getJSONObject(i);
						arrayECard.add(object.getString("main_url"));
					}

					if (isLast) {
						dialog.dismiss();
						Random r = new Random();
						if (arrayECard.size() > 8) {
							arrayPopularECard = new ArrayList<String>();
							for (int i = 0; i < 8; i++) {

								arrayPopularECard.add(arrayECard.get(r.nextInt(arrayECard.size())));

							}
							gridPopularECard.setAdapter(new PopularEmojiAdapter(getActivity(), arrayPopularECard));
						}
					}
				} else {
					dialog.dismiss();
					// handle false condition
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();

			}
		}

	}

	public class PopularEmojiAdapter extends BaseAdapter {

		FragmentActivity activity;
		ArrayList<String> arrayPopularECard;

		public PopularEmojiAdapter(FragmentActivity activity, ArrayList<String> arrayPopularECard) {
			// TODO Auto-generated constructor stub
			this.activity = activity;
			this.arrayPopularECard = arrayPopularECard;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arrayPopularECard.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return arrayPopularECard.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View v = LayoutInflater.from(activity).inflate(R.layout.popular_ecard_item, null);

			ImageView imageEmoji = (ImageView) v.findViewById(R.id.imageECard);
			Picasso.with(activity).load(arrayPopularECard.get(position)).placeholder(R.drawable.app_icon_one)
					.error(R.drawable.not_found).into(imageEmoji);
			imageEmoji.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(getActivity(), PopularEmojiDetailActivity.class);
					i.putExtra("ecardurl", arrayPopularECard.get(position));
					startActivity(i);
				}
			});
			return v;
		}

	}

	public static Fragment newInstance() {
		// TODO Auto-generated method stub
		PopularEmojiFragment fragmentFirst = new PopularEmojiFragment();

		return fragmentFirst;
	};

}
