package com.bibleecard;

public class ImgData {
private String imgName;
private String imgThumbUrl;
private String imgMainUrl;
private String linkUrl;
public ImgData(String string,String string2,String string3,String string4)
{
	imgName=string;
	imgThumbUrl=string2;
	imgMainUrl=string3;
	linkUrl=string4;
}
public String getLinkUrl() {
	return linkUrl;
}
public void setLinkUrl(String linkUrl) {
	this.linkUrl = linkUrl;
}
public String getImgName() {
	return imgName;
}
public void setImgName(String imgName) {
	this.imgName = imgName;
}
public String getImgThumbUrl() {
	return imgThumbUrl;
}
public void setImgThumbUrl(String imgThumbUrl) {
	this.imgThumbUrl = imgThumbUrl;
}
public String getImgMainUrl() {
	return imgMainUrl;
}
public void setImgMainUrl(String imgMainUrl) {
	this.imgMainUrl = imgMainUrl;
}
}
