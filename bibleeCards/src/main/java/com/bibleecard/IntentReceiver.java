
package com.bibleecard;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.urbanairship.push.BaseIntentReceiver;
import com.urbanairship.push.PushMessage;

public class IntentReceiver extends BaseIntentReceiver {

	private static final String TAG = "IntentReceiver";

	@Override
	protected void onChannelRegistrationSucceeded(Context context, String channelId) {
		Log.i(TAG, "Channel registration updated. Channel Id:" + channelId + ".");

		// Broadcast that the channel updated. Used to refresh the channel ID on
		// the main activity.
		LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MainActivity.ACTION_UPDATE_CHANNEL));
	}

	@Override
	protected void onChannelRegistrationFailed(Context context) {
		Log.i(TAG, "Channel registration failed.");
	}

	@Override
	protected void onPushReceived(Context context, PushMessage message, int notificationId) {
		Log.i(TAG, "Received push notification. Alert: " + message.getAlert() + ". Notification ID: " + notificationId);
	}

	@Override
	protected void onBackgroundPushReceived(Context context, PushMessage message) {
		Log.i(TAG, "Received background push message: " + message);
	}

	@Override
	protected boolean onNotificationOpened(Context context, PushMessage message, int notificationId) {
		Log.i(TAG, "User clicked notification. Alert: " + message.getAlert());

		Intent intent = new Intent("com.urbanairship.actions.SHOW_LANDING_PAGE_INTENT_ACTION");
		intent.setClass(context, SplashActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		context.startActivity(intent);

		// Return false here to allow Urban Airship to auto launch the launcher
		// activity
		return false;
	}

	@Override
	protected boolean onNotificationActionOpened(Context context, PushMessage message, int notificationId,
			String buttonId, boolean isForeground) {
		Log.i(TAG, "User clicked notification button. Button ID: " + buttonId + " Alert: " + message.getAlert());

		// Return false here to allow Urban Airship to auto launch the launcher
		// activity for foreground notification action buttons
		return false;
	}

	@Override
	protected void onNotificationDismissed(Context context, PushMessage message, int notificationId) {
		Log.i(TAG, "Notification dismissed. Alert: " + message.getAlert() + ". Notification ID: " + notificationId);
	}
}
