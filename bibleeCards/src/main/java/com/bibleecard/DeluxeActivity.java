package com.bibleecard;

import org.json.JSONArray;
import org.json.JSONObject;

import com.facebook.appevents.AppEventsLogger;
import com.rkp.billing.IabHelper;
import com.rkp.billing.IabResult;
import com.rkp.billing.Purchase;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;
import pooja.rk.love.UserEmailFetcher;

public class DeluxeActivity extends Activity implements Constant_values {

	JSONObject jsonObject;
	ImageView imageDeluxe;

	String sku_id;
	// private static final String PRODUCT_ID = "biblyfy.your.screen.paid.one";
	private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAirdnkErMKAs/hz/zhw/696qsO8KmautpjoN6zGmNecqWSRkbJAXqFE8rXuVCyCxw94/vgQdkiZ8zTpFvRBds+zjTN9WrTgMYd8BX/PupA3LkVY3JPVbUPE+RhLCbVhf7c5dx9lg/wFQ+I0K5E8YasWsMpqbCpknyVH/HjymEgndOygddApPUpGA9/ngNEkM2bh+aEwsEBXtT/KyuPrR1dvIcr+PRzsAVTIGQw/0A3qSX5w+T/peVo8uoRrVvew5h+RASY0SnDSDio6Hch00Lt9eftI9XlGl/a6IXhzPapNYkJSxsuzOztJAVRARe+dt5XFQ7ya5DKH/Ak4lTb8YVbwIDAQAB"; // PUT
	// The helper object
	IabHelper mHelper;
	// Debug tag, for logging
	static final String TAG = "BIBLEECARDAPP";
	// check ready or not
	private boolean is_ready = false;

	// (arbitrary) request code for the purchase flow
	static final int RC_REQUEST = 1505;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.deluxe_activity);

		getSubscriptionInfo();

		imageDeluxe = (ImageView) findViewById(R.id.imageDeluxe);
		imageDeluxe.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (is_ready) {
					System.out.println("clicked");
					mHelper.launchPurchaseFlow(DeluxeActivity.this, sku_id, RC_REQUEST, mPurchaseFinishedListener);
				}
			}
		});
		mHelper = new IabHelper(this, LICENSE_KEY);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				Log.d(TAG, "Setup finished.");
				System.out.println("Setup finished.");
				if (!result.isSuccess()) {
					// Oh noes, there was a problem.
					Toast.makeText(DeluxeActivity.this, "Problem setting up in-app billing: " + result,
							Toast.LENGTH_LONG).show();
					is_ready = false;
					return;
				}

				// Have we been disposed of in the meantime? If so, quit.
				if (mHelper == null) {
					return;
				}

				// IAP is fully set up. Now, let's get an inventory of stuff we
				// own.
				Log.d(TAG, "Setup successful. Querying inventory.");
				is_ready = true;
				// mHelper.queryInventoryAsync(mGotInventoryListener);
			}
		});
	}

	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
			// if we were disposed of in the meantime, quit.
			if (mHelper == null)
				return;

			if (result.isFailure()) {
				return;
			}
			Log.d(TAG, "Purchase successful.");
			AppEventsLogger appEventsLogger = AppEventsLogger.newLogger(getApplicationContext(),
					getResources().getString(R.string.FB_APP_ID));
			appEventsLogger.logEvent("category_purchased");
			upgradeItem();
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (mHelper != null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
			// not handled, so handle it ourselves (here's where you'd
			// perform any handling of activity results not related to in-app
			// billing...
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		// if (mAdView != null) {
		// mAdView.destroy();
		// }
		super.onDestroy();
		// very important:
		Log.d(TAG, "Destroying helper.");
		if (mHelper != null) {
			mHelper.dispose();
			mHelper = null;
		}
	}

	public void getSubscriptionInfo() {
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				jsonObject = new HttpPost_Parser().get(Constant_values.SUBSCRIPTION);
				return null;
			}

			protected void onPostExecute(Void result) {
				try {
					if (jsonObject.getBoolean("success")) {

						JSONArray array = jsonObject.optJSONArray("result");
						if (array != null && array.length() > 0) {
							JSONObject object = array.optJSONObject(0);
							String subscription_id = object.optString("subscription_id");
							sku_id = object.optString("sku_id");
							String subscription_title = object.optString("subscription_title");

						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			};
		}.execute();
	}

	public void upgradeItem() {
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				String email = UserEmailFetcher.getEmail(DeluxeActivity.this);
				jsonObject = new HttpPost_Parser().get(SUBSCRIPTION_ADD_To_DATABASE + email);
				return null;
			}

			protected void onPostExecute(Void result) {
				try {
					if (jsonObject.getBoolean("success")) {
						Toast.makeText(DeluxeActivity.this, "Successfully purchased", Toast.LENGTH_SHORT).show();
						Intent intent = new Intent(DeluxeActivity.this, Home.class);
						startActivity(intent);
						finish();
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			};
		}.execute();
	}
}
