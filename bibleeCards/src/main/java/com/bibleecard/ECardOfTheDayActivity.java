package com.bibleecard;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import pooja.rk.love.TransparentProgressDialog;

public class ECardOfTheDayActivity extends Activity implements Constant_values {

	private Calendar cal;
	private TextView tv;
	RelativeLayout relativeAppOfTheDay;
	Button buttonSend, buttonViewMore;
	ImageView imageView;
	private InterstitialAd appoftheday_interstitial;

	ArrayList<String> arrayCategoryId;
	ArrayList<String> arrayEmoji;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ecardoftheday_activity);
		NetworkUtil.isNotification = true;
		cal = Calendar.getInstance();
		tv = (TextView) findViewById(R.id.textView2);
		tv.setText("" + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " "
				+ cal.get(Calendar.DAY_OF_MONTH) + " " + cal.get(Calendar.YEAR));

		relativeAppOfTheDay = (RelativeLayout) findViewById(R.id.relativeAppOfTheDay);

		buttonSend = (Button) findViewById(R.id.buttonSend);
		buttonViewMore = (Button) findViewById(R.id.buttonViewMore);

		imageView = (ImageView) findViewById(R.id.imageView);
		load_todayApp();
		if (NetworkUtil.isOnline()) {
			new GetECardOfTheDay().execute();
		} else {
			Toast.makeText(ECardOfTheDayActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
		}

		relativeAppOfTheDay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (appoftheday_interstitial.isLoaded()) {
					appoftheday_interstitial.show();
				}
			}
		});
		buttonSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					imageView.buildDrawingCache();
					Bitmap bmap = imageView.getDrawingCache();
					// sharing eomoji
					if (bmap != null) {
						String pathofBmp = Images.Media.insertImage(getContentResolver(), bmap, "", null);
						// saveImage(bmp);
						Uri bmpUri = Uri.parse(pathofBmp);
						if (bmpUri != null) {
							final Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
							emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							emailIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);
							emailIntent1.putExtra(Intent.EXTRA_TEXT, "http://www.FunBibleEcards.com/");
							emailIntent1.setType("image/png");
							startActivity(Intent.createChooser(emailIntent1, "Share via..."));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		buttonViewMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(ECardOfTheDayActivity.this, Catagories.class));
			}
		});
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		NetworkUtil.isNotification = false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	private class GetECardOfTheDay extends AsyncTask<Void, Void, Void> {
		JSONObject jsonobject;
		TransparentProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new TransparentProgressDialog(ECardOfTheDayActivity.this, R.drawable.progress);
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			jsonobject = new HttpPost_Parser().get(ecard_of_the_day);
			return null;
		}

		@SuppressLint("DefaultLocale")
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				// 11/26/2015
				if (jsonobject.getBoolean("success")) {
					JSONArray jsonArray = jsonobject.getJSONArray("result");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						String img_id = object.getString("img_id");
						String emoji_of_day_date = object.getString("ecard_of_day_date");
						String thumb_url = object.getString("thumb_url");
						String main_url = object.getString("main_url");

						System.out.println("Date===" + emoji_of_day_date);

						try {

							Calendar c = Calendar.getInstance();

							SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

							Date date1 = formatter.parse(emoji_of_day_date);

							String currentDate = formatter.format(c.getTime());

							Date date2 = formatter.parse(currentDate);

							System.out.println("date1" + date1);
							System.out.println("date2" + date2);

							if (date1.equals(date2)) {
								ImageLoader.getInstance().loadImage(main_url, new SimpleImageLoadingListener() {
									@SuppressLint("NewApi")
									@Override
									public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
										// TODO Auto-generated method stub
										imageView.setImageBitmap(loadedImage);
									}
								});
								break;
							} else {
								if (NetworkUtil.isOnline()) {
									new GetCatagories().execute();
								} else {
									Toast.makeText(ECardOfTheDayActivity.this, "No internet connection",
											Toast.LENGTH_LONG).show();
								}
							}

						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					}

				} else {
					// handle exception condition here
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private class GetCatagories extends AsyncTask<Void, Void, Void> {
		JSONObject jsonobject;
		TransparentProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new TransparentProgressDialog(ECardOfTheDayActivity.this, R.drawable.progress);
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			jsonobject = new HttpPost_Parser().get(all_catagories_parser);
			return null;
		}

		@SuppressLint("DefaultLocale")
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (jsonobject.getBoolean("success")) {
					arrayCategoryId = new ArrayList<String>();
					JSONArray jsonArray = jsonobject.getJSONArray("result");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						String id = object.getString("category_id");

						String type = object.getString("type").toUpperCase();
						if (type.equals("FREE")) {
							arrayCategoryId.add(id);
						}
					}

					Random r = new Random();

					System.out.println("CategorySize===" + arrayCategoryId.size());
					System.out.println("RandomCategoryID===" + arrayCategoryId.get(r.nextInt(arrayCategoryId.size())));

					if (NetworkUtil.isOnline()) {
						new Get_Cat_Images(arrayCategoryId.get(r.nextInt(arrayCategoryId.size()))).execute();
					} else {
						Toast.makeText(ECardOfTheDayActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
					}
				} else {
					// handle exception condition here
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private class Get_Cat_Images extends AsyncTask<Void, Void, Void> {
		JSONObject json;
		TransparentProgressDialog progressDialog;
		String categoryId;

		public Get_Cat_Images(String categoryId) {
			// TODO Auto-generated constructor stub
			this.categoryId = categoryId;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new TransparentProgressDialog(ECardOfTheDayActivity.this, R.drawable.progress);
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			json = new HttpPost_Parser().get(selected_cat_parser + categoryId + part + "free");
			System.out.println(selected_cat_parser + categoryId + part + "free");
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				if (json.getBoolean("success")) {
					arrayEmoji = new ArrayList<String>();
					JSONArray array = json.getJSONArray("result");
					for (int i = 0; i < array.length(); i++) {
						JSONObject object = array.getJSONObject(i);
						arrayEmoji.add(object.getString("main_url"));
					}

					if (arrayEmoji.size() > 0) {
						Random r = new Random();

						System.out.println("RandomCategoryID===" + arrayEmoji.get(r.nextInt(arrayEmoji.size())));
						ImageLoader.getInstance().loadImage(arrayEmoji.get(r.nextInt(arrayEmoji.size())),
								new SimpleImageLoadingListener() {
									@SuppressLint("NewApi")
									@Override
									public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
										// TODO Auto-generated method stub
										imageView.setImageBitmap(loadedImage);
									}
								});
					}
				} else {
					// handle false condition
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();

			}
		}
	}

	public void load_todayApp() {
		appoftheday_interstitial = new InterstitialAd(ECardOfTheDayActivity.this);
		appoftheday_interstitial.setAdUnitId("ca-app-pub-1979658376370758/5817244224");

		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

		// Begin loading your interstitial.
		appoftheday_interstitial.loadAd(adRequestBuilder.build());
	}
}
