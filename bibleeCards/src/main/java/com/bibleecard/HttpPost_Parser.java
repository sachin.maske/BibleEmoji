package com.bibleecard;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

public class HttpPost_Parser {
	private InputStream in = null;
	private String json = "";
	private JSONObject jobj;

	public JSONObject get(String registerUserUrl) {
		System.out.println(registerUserUrl);
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(registerUserUrl);
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpResponse.getEntity();
			in = httpEntity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			in.close();
			json = sb.toString();
			System.out.println("=====json returned---------" + json);
			jobj = new JSONObject(json);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jobj;
	}
}
