package com.bibleecard;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;

public class TodaySponserFragment extends Fragment {
	NativeExpressAdView mNativeExpressAdView;

	public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container,
			android.os.Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_todaysponser, container, false);

		LinearLayout layout = (LinearLayout) view.findViewById(R.id.linearAds);

		// Create a native express ad. The ad size and ad unit ID must be set
		// before calling
		// loadAd.
		mNativeExpressAdView = new NativeExpressAdView(getActivity());
		mNativeExpressAdView.setAdSize(new AdSize(AdSize.FULL_WIDTH, 480));
		mNativeExpressAdView.setAdUnitId("ca-app-pub-1979658376370758/2000837421");
		// Create an ad request.
		AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		 adRequestBuilder.addTestDevice("2FB882443B07AD2E8BFE4CBCECE28556");

		// Add the NativeExpressAdView to the view hierarchy.
		layout.addView(mNativeExpressAdView);

		// Start loading the ad.
		mNativeExpressAdView.loadAd(adRequestBuilder.build());

		return view;
	}

	public static Fragment newInstance() {
		// TODO Auto-generated method stub
		TodaySponserFragment fragmentFirst = new TodaySponserFragment();

		return fragmentFirst;
	};

}
