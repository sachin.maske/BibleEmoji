package com.bibleecard;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

public class PopularEmojiDetailActivity extends Activity {

	ImageView imageECard;
	Button button;
	String ecardiurl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.popular_ecard_detail_activity);

		ecardiurl = getIntent().getStringExtra("ecardurl");

		imageECard = (ImageView) findViewById(R.id.imageECard);
		button = (Button) findViewById(R.id.button5);
		ImageLoader.getInstance().loadImage(ecardiurl, new SimpleImageLoadingListener() {
			@SuppressLint("NewApi")
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				// TODO Auto-generated method stub
				imageECard.setImageBitmap(loadedImage);
				// System.out.println(bmp.getByteCount());
				button.setEnabled(true);
			}
		});
	}

	public void goBack(View v) {
		finish();
	}

	@SuppressLint("NewApi")
	public void setWallpaper(View view) {
		try {
			imageECard.buildDrawingCache();
			Bitmap bmap = imageECard.getDrawingCache();
			// sharing eomoji
			if (bmap != null) {
				final Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
				emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				emailIntent1.putExtra(Intent.EXTRA_TEXT, "http://www.FunBibleEcards.com/");
				emailIntent1.setType("image/png");
				startActivity(Intent.createChooser(emailIntent1, "Share via ..."));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
